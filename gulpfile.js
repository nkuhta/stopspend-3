var elixir = require('laravel-elixir');
var gulp = require('gulp');
var run = require('gulp-run');
var coffee = require('gulp-coffee');
var os = require('os');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
//
// elixir(function(mix) {
//     mix.sass('app.scss');
// });
//
// gulp.task('watch', function () {
//     gulp.watch('client/desktop/templates/*', ['copy-templates']);
//     gulp.watch('client/mobile/templates/*', ['copy-templates']);
// });


gulp.task('coffee-compile', function() {
    gulp.src('./client/**/*.coffee')
        .pipe(coffee({bare: true}))
        .pipe(gulp.dest('./public/build/'));
});

gulp.task('copy-templates', function () {
    if (os.platform() === 'win32') {
        run('copy_mustache.bat').exec();
    } else {
        run('sh copy_templates.sh').exec();
    }
});

gulp.task('build', ['coffee-compile', 'copy-templates']);