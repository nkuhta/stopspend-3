<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Socket implements ShouldBroadcast
{
    public static function emit($channel, $event, $data = []) {
        event(new self($channel, $event, $data));
    }

    use SerializesModels;
    public $data;
    public function __construct($channel, $event, $data)
    {
        $this->data = [
            'channel' => $channel,
            'event' => $event,
            'data' => $data,
        ];
    }

    public function broadcastOn()
    {
        return ['events'];
    }
}
