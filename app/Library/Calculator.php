<?php

namespace App\Library;

use App\Model\Month;
use App\Model\Operation;
use \DateTime;
use \DateInterval;

class Calculator
{
    protected $month;
    protected $days = [];
    protected $daysCount = 0;
    protected $plans = [];
    protected $planSumm = 0;
    protected $canSpendPerDay = 0;
    protected $totalOutcome = 0;
    protected $monthBalance = 0;
    public function __construct(Month $month)
    {
        $this->month = $month;
        $this->calculate();
    }

    public function isCurrentMonth()
    {
        return $this->month->isCurrentMonth();

    }

    public function isFutureMonth()
    {
        return $this->month->isFutureMonth();
    }

    public function isPastMonth()
    {
        return $this->month->isPastMonth();
    }

    public function initializePlans()
    {
        foreach($this->month->plans()->get() as $plan) {
            if (empty($this->plans[$plan->category_id])) {
                $this->plans[$plan->category_id] = 0;
            }
            $amount = $plan->amount;
            $this->plans[$plan->category_id] += $amount;
            $this->planSumm += $amount;
        }
    }

    public function initializeDays()
    {
        $start = new DateTime($this->month->date . '-01');
        $end = clone($start);
        $end = $end->add(new DateInterval('P1M'))->sub(new DateInterval('P1D'));
        while ($start <= $end) {
            $this->days[$start->format('Y-m-d')] = (object)[
                'spent' => 0,
                'balance' => 0
            ];
            $this->daysCount++;
            $start = $start->add(new DateInterval('P1D'));
        }
    }

    public function countCanSpendPerDay()
    {
        $income = $this->month->income;
        return (($income - $this->planSumm) / $this->daysCount);
    }

    protected function getPlanPartOfOperation(Operation $operation)
    {
        $category = $operation->category_id;
        $amount = $operation->amount;
        $plan = isset($this->plans[$category]) ? $this->plans[$category] : 0;
        $result = 0;

        if ($plan == 0) {
            $result = 0;
        } else if ($amount < $plan) {
            $result = $amount;
        } else {
            $result = $plan;
        }
        if (isset($this->plans[$category])) {
            $this->plans[$category] -= $result;
        }
        return $result;
    }

    public function countOperations()
    {
        # Идем по каждой операции, рассчитываем, плановая ли она или фактическая
        foreach($this->month->operations()->get() as $operation) {
            $planPart = $this->getPlanPartOfOperation($operation);
            if ($operation->is_credit) {
                continue;
            }
            $this->totalOutcome += $operation->amount;
            # У каждой операции теперь есть значение её плановой части
            # найдем день операции и добавим туда расход
            $date = date('Y-m-d', strtotime($operation->dt));
            $day = $this->days[$date];
            $factPlan = $operation->amount - $planPart;
            $day->spent += $factPlan;
        }
    }

    public function countDaysBalance()
    {
        $firstDay = true;
        $lastBalance = 0;
        $inheritedBalance = $this->month->getInheritedBalance();
        foreach ($this->days as $dt =>  $day) {
            if ($firstDay && $inheritedBalance) {
                $day->balance += $inheritedBalance;
            }
            $firstDay = false;
            $day->initialBalance = $day->balance += $lastBalance + $this->canSpendPerDay;
            $day->balance -= $day->spent;
            $lastBalance = $day->balance;
            $this->monthBalance = $lastBalance;
        }
    }

    public function countHealth()
    {
        $monthDate = date('Y-m-d', strtotime($this->month->date . '-01'));
        if ($this->isFutureMonth()) {
            $this->health = 0;
        } else {
            $positiveDays = 0;
            $totalDays = 0;
            if ($this->isCurrentMonth()) {
                foreach($this->days as $date => $day) {
                    if (strtotime($date) < time()) {
                        $totalDays++;
                        if ($day->balance > 0) {
                            $positiveDays++;
                        }
                    }
                }
            } else if ($this->isPastMonth()) {
                $totalDays = date('t', strtotime($monthDate));
                foreach($this->days as $date => $day) {
                    if ($day->balance > 0) {
                        $positiveDays++;
                    }
                }
            }
            $this->health = round($positiveDays / $totalDays * 100);
        }
    }


    public function calculate()
    {
        $this->initializePlans();
        $this->initializeDays();
        $this->canSpendPerDay = $this->countCanSpendPerDay();
        $this->countOperations();
        $this->countDaysBalance();
        $this->countHealth();
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function getMonthBalance()
    {
        if (!$this->isPastMonth()) {
            return null;
        }
        return $this->monthBalance;
    }
}