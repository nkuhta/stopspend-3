<?php

namespace App;

use App\Model\Space;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getSpace() {
        if ($this->space_id) {
            $space = Space::find($this->space_id);
        } else {
            $space = $this->hasOne('App\Model\Space', 'author_id')->first();
        }
        if (!$space) {
            $space = Space::create([
                'author_id' => $this->id
            ]);
            $space->save();
        }
        return $space;
    }

    public function setSpace($id)
    {
        $this->space_id = $id;
        $this->save();
    }

    public function getInvitationsAsInviter()
    {
        return $this->hasMany('App\Model\Invitation', 'inviter', 'id');
    }

    public function getInvitationsAsInvited()
    {
        return $this->hasMany('App\Model\Invitation', 'invited', 'id');
    }

    public function onRegister()
    {
        $user = $this;
        \Mail::queue('emails.registered', [], function ($message) use ($user) {
            $message->to($user->email)->subject('Вы были зарегистрированы в Stopspend.ru');
        });
    }

    public function onPasswordChange()
    {
        $user = $this;
        \Mail::queue('emails.password_change', [], function ($message) use ($user) {
            $message->to($user->email)->subject('Пароль в сервисе Stopspend.ru изменен');
        });
    }

    public function onPasswordReset($password)
    {
        $user = $this;
        \Mail::queue('emails.password_reset', ['password' => $password], function ($message) use ($user) {
            $message->to($user->email)->subject('Пароль в сервисе Stopspend.ru сброшен');
        });
    }

}
