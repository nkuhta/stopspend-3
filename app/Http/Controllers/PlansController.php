<?php


namespace App\Http\Controllers;

use App\Events\Socket;
use App\Model\Month;
use App\Model\Operation;
use App\Model\Plan;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;

class PlansController extends Controller
{
    public function get($month_id) {
        $month = Month::find($month_id);
        if (!$month->belongsToUser()) {
            throw new Exception("Month not belogs to you");
        }

        return $month->plans()->get();
    }


    public function post(Request $request) {
        $this->validate($request, [
            'month_id' => 'required|numeric',
            'category' => 'required',
            'amount' => 'required|numeric',
        ]);
        $data = $request->input();
        $month = Month::find($data['month_id']);
        if (!$month->belongsToUser()) {
            throw new Exception("Month not belogs to you");
        }
        $space = \Auth::user()->getSpace();
        $category = $space->getCategory($data['category']);

        $data['amount'] = (float)$data['amount'];
        $data['category_id'] = $category->id;
        $data['author_id'] = \Auth::user()->id;
        $plan = Plan::create($data);
        $plan->save();
        return response()->ok($plan);
    }

    public function put(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric',
            'month_id' => 'numeric',
            'category' => '',
            'amount' => 'numeric',
        ]);
        $data = $request->input();
        unset($data['_token']);
        $plan = Plan::find($data['id']);
        if (!$plan || !$plan->belongsToUser()) {
            throw new Exception("Plan not belongs to you");
        }
        $data['amount'] = (float)$data['amount'];
        foreach($data as $k => $v) {
            $plan->$k = $v;
        }
        $plan->save();
        return response()->ok($plan);
    }

    public function delete (Request $request)
    {
        $id = $request->input('id');
        $plan = Plan::find($id);
        if (!$plan || !$plan->belongsToUser()) {
            throw new Exception('Plan not belongs to user');
        }
        $plan->delete();
        return response()->ok(true);
    }

    public function copyPlansFromPreviousMonth(Request $request)
    {
        $id = $request->input('month_id');
        $month = Month::find($id);
        if (!$month || !$month->belongsToUser()) {
            throw new Exception("Month not belogs to you");
        }
        $previousMonth = $month->getPreviousMonth();
        if (!$previousMonth) {
            throw new \Exception('Предыдущий месяц не найден. Копирование невозможно.');
        }
        $plans = $previousMonth->plans()->get();
        if (!count($plans)) {
            throw new \Exception('В предыдущем месяце нет планов. Нечего копировать.');
        }
        // Удаляем планы этого месяца
        foreach($month->plans()->get() as $plan) {
            $plan->delete();
        }
        foreach($plans as $plan) {
            $data = $plan->toArray();
            $data['author_id'] = \Auth::user()->id;
            $data['month_id'] = $month->id;
            $planInstance = new Plan($data);
            $planInstance->save();
        }
        $month->plan_copied = 1;
        $month->save();
        return response()->ok(true);
    }

    public function setPlanCopied(Request $request)
    {
        $id = $request->input('month_id');
        $month = Month::find($id);
        if (!$month || !$month->belongsToUser()) {
            throw new Exception("Month not belogs to you");
        }
        $value = $request->input('value');
        if ($value !== -1 && $value !== 1) {
            throw new \Exception('Value is invalid');
        }
        $month->plan_copied = $value;
        $month->save();
        return response()->ok(true);

    }
}