<?php


namespace App\Http\Controllers;

use App\Events\Socket;
use App\Exceptions\Error;
use App\Model\Category;
use App\Model\Month;
use App\Model\Operation;
use App\Model\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\CountValidator\Exception;

class CategoriesController extends Controller
{
    public function get() {
        $space = \Auth::user()->getSpace();
        return $space->categories()->get();
    }


    public function post(Request $request) {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $data = $request->input();
        $space = Auth::user()->getSpace();
        $category = $space->getCategory($data['name']);
        return response()->ok($category);
    }

    public function put(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $data = $request->input();
        unset($data['_token']);
        $category = Category::find($data['id']);
        if (!$category->belongsToUser()) {
            throw new Exception('Category not belongs to user');
        }
        $category->name = $data['name'];
        $category->save();
        return response()->ok($category);
    }

    public function delete (Request $request)
    {
        $id = $request->input('id');
        $category = Category::find($id);
        if (!$category->belongsToUser()) {
            throw new Exception('Category not belongs to user');
        }
        if ($category->linkedWithSomething()) {
            throw new Error("Категория связана с планом или с операциями. Удаление невозможно.");
        }
        $category->delete();
        return response()->ok(true);

    }
}