<?php

namespace App\Http\Controllers;

use App\Events\Socket;
use App\Model\Month;
use App\Model\Operation;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;

class OperationsController extends Controller
{
    public function get($month_id) {
        $month = Month::find($month_id);
        if (!$month->belongsToUser()) {
            throw new Exception('Month not belongs to you!');
        }
        $items = $month->operations()->get();
        return $items;
    }


    public function post(Request $request) {
        $this->validate($request, [
            'month_id' => 'required|numeric',
            'category' => 'required',
            'amount' => 'required|numeric',
            'dt' => 'required|date',
            'is_credit' => 'required|boolean'
        ]);
        $data = $request->input();
        if (!$data['dt']) {
            $data['dt'] = date('Y-m-d H:i:s');
        }
        $month = Month::find($data['month_id']);
        if (!$month->belongsToUser()) {
            throw new Exception('Month not belongs to you!');
        }

        $data['dt'] = date('Y-m-d H:i:s', strtotime($data['dt'])); // Конвертируем дату
        $min = (new \DateTime($month->date . '-01 00:00:00'));
        $max = clone($min);
        $max = $max->add(new \DateInterval('P1M'))->format('Y-m-d H:i:s');
        $min = $min->sub(new \DateInterval('PT1S'))->format('Y-m-d H:i:s');

        $this->validate($request, [
            'dt' => 'required|date|after:' . $min . '|before:' . $max
        ]);


        $space = \Auth::user()->getSpace();
        $category = $space->getCategory($data['category']);

        $data['amount'] = (float)$data['amount'];
        $data['category_id'] = $category->id;
        $data['author_id'] = \Auth::user()->id;
        $operation = Operation::create($data);
//        $operation->save();
        return response()->ok($operation);

    }

    public function put(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric',
            'amount' => 'numeric',
            'dt' => 'date',
            'is_credit' => 'required|boolean'
        ]);

        $data = $request->input();
        $data['dt'] = date('Y-m-d H:i:s', strtotime($data['dt'])); // Конвертируем дату
        unset($data['_token']);
        $operation = Operation::find($data['id']);
        if (!$operation || !$operation->belongsToUser()) {
            throw new Exception('Month not belongs to you!');
        }
        $data['amount'] = (float)$data['amount'];
        foreach($data as $k => $v) {
            $operation->$k = $v;
        }
        $operation->save();
        return response()->ok($operation);
    }

    public function delete (Request $request)
    {
        $id = $request->input('id');
        $operation = Operation::find($id);
        if (!$operation || !$operation->belongsToUser()) {
            throw new Exception('Operation not belongs to user');
        }
        $operation->delete();
        return response()->ok(true);
    }
}
