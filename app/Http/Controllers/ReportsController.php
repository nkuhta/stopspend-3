<?php


namespace App\Http\Controllers;

use App\Events\Socket;
use App\Model\Month;
use App\Model\Operation;
use App\Model\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mockery\CountValidator\Exception;

class ReportsController extends Controller
{
    public function prepareReport($type)
    {
        $user = \Auth::user();
        $space = $user->getSpace();
        $data = [];
        $query = $space->categories();
        $data = Input::all();
        $start = null;
        $end = null;
        if ($data['month'] === 'current_month') {
            $start = date('Y-m-01 00:00:00');
            $end = date('Y-m-t 23:59:59');
        } else if ($data['month'] === 'previous_month') {
            $now = new \DateTime();
            $now->sub(new \DateInterval('P01M'));
            $start = $now->format('Y-m-01 00:00:00');
            $end = $now->format('Y-m-t 23:59:59');
        } else if ($data['month'] === 'manual') {
            $start = '1970-01-01';
            $end = '1970-01-01';
        }
        foreach($query->get() as $category) {
            $query = Operation::where('category_id', $category->id);
            if ($start) {
                $query = $query->where('dt', '>=', $start);
            }
            if ($end) {
                $query = $query->where('dt', '<=', $end);
            }
            $summ = $query->sum('amount');

            if ($summ > 0) {
                $data[] = [
                    'name' => $category->name,
                    'summ' => $summ
                ];
            }
        }
        return response()->ok($data);
    }
}