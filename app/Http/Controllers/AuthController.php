<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginView()
    {
        return view('auth/login');
    }

    public function login()
    {
        $input = \Request::input();
        if (Auth::attempt([
            'email' => $input['email'],
            'password' => $input['password'],
        ])) {
            return redirect()->action('MainController@getIndex');
        } else {
            return redirect()->to('/login/')->withErrors('Пользовтель или пароль не верный');
        }
    }

    public function logout()
    {
        \Session::flush();
        return redirect()->to('/');
    }

    public function registration()
    {
        return view('auth/registration');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'password_again' => 'required|same:password'
        ]);
        $data = $request->input();
        $data['password'] = \Hash::make($data['password']);
        $user = User::create($data);
        $user->save();
        $user->getSpace();
        $user->onRegister();
        return redirect()->to('/');
    }

    public function passwordRecovery()
    {
        return view('auth/password_recovery');
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);
        $email = $request->input('email');
        $user = User::where('email', $email)->first();
        if (!$user) {
            return redirect()->action('AuthController@passwordRecovery')->withErrors('Пользователь с таким адресом не найден');
        }

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&()?";
        $password = substr( str_shuffle( $chars ), 0, 8 );
        $hashed_password = \Hash::make($password);
        $user->password = $hashed_password;
        $user->save();
        $user->onPasswordReset($password);
        Session::flash('password_reset_message', 'Пароль сброшен и выслан на электронную почту');
        return redirect()->action('AuthController@passwordRecovery');


    }
}