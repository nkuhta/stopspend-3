<?php


namespace App\Http\Controllers;

use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function get()
    {
        return view('profile')->with('user', \Auth::user());
    }

    public function post(Request $request)
    {
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $destination = storage_path(
                'avatars'
            );
            $f = $file->move($destination, date('YmdHis') . rand(0, 100) . '.' . $file->getClientOriginalExtension());
            // Папка для маленьких картинок
            $miniDir = $f->getPath() . DIRECTORY_SEPARATOR . 'mini';
            if (!is_dir($miniDir)) {
                mkdir($miniDir);
            }
            $miniPath = $miniDir . DIRECTORY_SEPARATOR . $f->getFilename();

            Image::make($f->getPathname(), [
                'width' => 150,
                'height' => 150,
                'crop' => true
            ])->save($f->getPathname());

            // Делаем очень маленькую каринку
            Image::make($f->getPathname(), [
                'width' => 30,
                'height' => 30,
                'crop' => true
            ])->save($miniPath);

            $user = \Auth::user();
            $user->avatar = $f->getFilename();
            $user->save();
        }
        return redirect()->to('/profile');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6',
            'password_again' => 'required|same:password'
        ]);
        $password = \Hash::make($request->input('password'));
        $user = \Auth::user();
        $user->password = $password;
        $user->save();
        $user->onPasswordChange();
        $request->session()->flash('password_reset_message', 'Пароль был изменен успешно');
        return redirect()->to('profile');
    }
}