<?php

namespace App\Http\Controllers;

use App\Events\Socket;
use App\Model\Month;
use Illuminate\Http\Request;

class MonthController extends Controller
{
    public function getMonth($month_date) {
        $user = \Auth::user();
        $space = $user->getSpace();
        $month = $space->month()->where('date', $month_date)->first();
        if (!$month) {
            // Надо создать новый месяц, раз его нет
            $month = Month::create([
                'date' => $month_date,
                'author_id' => $user->id,
                'space_id' => $space->id,
            ]);
        }
        if (!$month->belongsToUser()) {
            throw new \Exception('Month not belongs to you');
        }

        // Если версия (мобильная или настольная) не выбрана, пока выбираем настольную
        if (!\Session::get('version')) {
            \Session::set('version', 'desktop');
        }

        return view('application/month', [
            'month' => $month
        ]);
    }

    public function getState($month_id) {
        $month = Month::find($month_id);
        if (!$month->belongsToUser(\Auth::user()->id)) {
            throw new \Exception('Month not belongs to you');
        }
        return $month->getState();
    }

    public function saveState(Request $request, $month_id) {
        $month = Month::find($month_id);
        if (!$month->belongsToUser(\Auth::user()->id)) {
            throw new \Exception('Month not belongs to you');
        }
        $data = $request->input();
        unset($data['_token']);
        foreach($data as $k => $v) {
            $month->$k = $v;
        }
        $month->save();
        return response()->ok();
    }
}
