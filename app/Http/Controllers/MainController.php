<?php

namespace App\Http\Controllers;

use App\Model\Invitation;
use App\Model\Month;

class MainController extends Controller
{
    public function getIndex()
    {
        if (\Auth::check()) {
            // Список месяцев
            $user = \Auth::user();
            $space = $user->getSpace();
            $months = $space->getMonths()->get();

            // СОбираем данные по месяцам, а так-же добавляем текущий и следующий, если их нет
            $hash = [];
            foreach($months as $month) {
                $date = $month->date;
                $hash[$date] = [
                    'health' => $month->getHealth(),
                    'date' => $month->getTitle(),
                    'balance' => $month->getBalance()
                ];
            }

            $first = date('Y-m'); // первый день этого месяца
            if (empty($hash[$first])) {
                $hash[$first] = [
                    'health' => 0,
                    'date' => strftime('%B %Y', strtotime($first.'-01')),
                    'balance' => $month->getBalance()
                ];
            }
            $next = (new \DateTime($first))->add(new \DateInterval('P1M'))->format('Y-m');
            if (empty($hash[$next])) {
                $hash[$next] = [
                    'health' => 0,
                    'date' => strftime('%B %Y', strtotime($next.'-01')),
                    'balance' => $month->getBalance()
                ]; // следующий месяц
            }

            ksort($hash);
            return view('space', [
                'months' => $hash,
                'space' => $space,
                'intitationsAsInviter' => $user->getInvitationsAsInviter()->get(),
                'intitationsAsInvited' => $user->getInvitationsAsInvited()->where([
                    'status' => 'new'
                ])->get(),
            ]);
        } else {
            return view('index');
        }
    }
}
