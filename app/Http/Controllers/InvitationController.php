<?php

namespace App\Http\Controllers;

use App\Model\Invitation;
use App\User;
use Illuminate\Http\Request;

class InvitationController extends Controller
{
    public function invite(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);
        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            throw new \Exception('User not found!');
        }
        if ($user->id === \Auth::user()->id) {
            throw new \Exception("User is you!");
        }

        $inviter = \Auth::user()->id;
        $invited = $user->id;
        $invitation = Invitation::where([
            'inviter' => $inviter,
            'invited' => $invited
        ])->first();
        if ($invitation) {
            throw new \Exception("Invitation already exists");
        }
        $invitation = Invitation::create([
            'inviter' => $inviter,
            'invited' => $invited,
            'status' => 'new'
        ])->save();

        return response()->ok([]);
    }

    public function accept(Request $request)
    {
        $this->validate($request, [
            'invitation' => 'required|integer'
        ]);
        $id = $request->input('invitation');
        $invitation = Invitation::find($id);
        if (!$invitation || $invitation->invited !== \Auth::user()->id) {
            throw new \Exception('Invitation is not yours!');
        }
        $invitation->perform();
        return response()->ok();
    }

    public function cancel(Request $request)
    {
        $this->validate($request, [
            'invitation' => 'required|integer'
        ]);
        $id = $request->input('invitation');
        $invitation = Invitation::find($id);
        if (!$invitation || $invitation->invited !== \Auth::user()->id) {
            throw new \Exception('Invitation is not yours!');
        }
        $invitation->cancel();
        return response()->ok();
    }

    public function leaveSpace()
    {
        $user = \Auth::user();
        $space = $user->getSpace();
        if ($space->author_id === $user->id) {
            throw new \Exception('Cannot leave own space');
        }

        if ($user->space_id) {
            $user->space_id = null;
            $user->save();
            return redirect()->to('/');
        } else {
            throw new \Exception('No space to leave');
        }

    }

}
