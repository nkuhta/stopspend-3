<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

$forceUrl = env('APP_FORCE_URL_ROOT', false);
if ($forceUrl) {
    URL::forceRootUrl($forceUrl);
}

Route::group(['middleware' => ['web']], function () {
    //
    Route::get('/', 'MainController@getIndex');

    Route::get('/login/', 'AuthController@loginView');
    Route::get('/registration/', 'AuthController@registration');
    Route::post('/login/', 'AuthController@login');
    Route::post('/register/', 'AuthController@register');
    Route::get('/logout/', 'AuthController@logout');
    Route::get('/password_recovery/', 'AuthController@passwordRecovery');
    Route::post('/reset_password/', 'AuthController@resetPassword');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/month/{date}', 'MonthController@getMonth');
        Route::get('/api/master/{id}', 'MonthController@getState');
        Route::post('/api/master/{id}', 'MonthController@saveState');

        Route::get('/api/operations/{month_id}', 'OperationsController@get');
        Route::post('/api/operations', 'OperationsController@post');
        Route::put('/api/operations', 'OperationsController@put');
        Route::delete('/api/operations', 'OperationsController@delete');

        Route::get('/api/plans/{month_id}', 'PlansController@get');
        Route::post('/api/plans', 'PlansController@post');
        Route::put('/api/plans', 'PlansController@put');
        Route::delete('/api/plans', 'PlansController@delete');
        Route::post('/api/copy_plans_from_prev', 'PlansController@copyPlansFromPreviousMonth');
        Route::post('/api/set_plan_copied', 'PlansController@setPlanCopied');

        Route::get('/api/categories', 'CategoriesController@get');
        Route::post('/api/categories', 'CategoriesController@post');
        Route::put('/api/categories', 'CategoriesController@put');
        Route::delete('/api/categories', 'CategoriesController@delete');

        Route::post('/invite', 'InvitationController@invite');
        Route::post('/invite/accept', 'InvitationController@accept');
        Route::post('/invite/cancel', 'InvitationController@cancel');
        Route::get('/leave_space', 'InvitationController@leaveSpace');

        Route::get('/profile', 'ProfileController@get');
        Route::post('/profile', 'ProfileController@post');
        Route::post('/profile/change_password', 'ProfileController@changePassword');

        Route::get('avatar/mini/{filename}', function ($filename) {
            $path = storage_path('avatars/mini') . '/' . $filename;
            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header('Content-Type', $type);
            return $response;
        });

        Route::get('avatar/{filename}', function ($filename) {
            $path = storage_path('avatars') . '/' . $filename;
            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header('Content-Type', $type);
            return $response;
        });

        Route::post('api/report/{type}', 'ReportsController@prepareReport');
    });


});
