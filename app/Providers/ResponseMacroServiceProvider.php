<?php namespace App\Providers;

use App\Exceptions\Error;
use Illuminate\Session\TokenMismatchException;
use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider {

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('ok', function($data = null)
        {
            return Response::json([
                'success' => true,
                'data' => $data ? $data : null
            ]);
        });

        Response::macro('exception', function (\Exception $e)
        {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
                'normal_error' => $e instanceof Error
            ]);
        });
    }

    public function register() {

    }

}