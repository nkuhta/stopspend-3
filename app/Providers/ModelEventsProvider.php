<?php

namespace App\Providers;

use App\Model\Category;
use App\Model\Operation;
use App\Model\Plan;
use Illuminate\Support\ServiceProvider;

class ModelEventsProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Operations

        Operation::created(function ($operation) {
            $operation->month()->emitSocketEvent('operation_created', $operation);
        });

        Operation::deleted(function ($operation) {
            $operation->month()->emitSocketEvent('operation_removed', $operation->id);
        });

        Operation::updated(function ($operation) {
            $operation->month()->emitSocketEvent('operation_updated', $operation);
        });


        // Plans

        Plan::created(function ($plan) {
            $plan->month()->emitSocketEvent('plan_created', $plan);
        });

        Plan::deleted(function ($plan) {
            $plan->month()->emitSocketEvent('plan_removed', $plan->id);
        });

        Plan::updated(function ($plan) {
            $plan->month()->emitSocketEvent('plan_updated', $plan);
        });

        // Categories

        Category::created(function ($category) {
            $category->space()->emitSocketEvent('category_created', $category);
        });

        Category::deleted(function ($category) {
            $category->space()->emitSocketEvent('category_removed', $category->id);
        });

        Category::updated(function ($category) {
            $category->space()->emitSocketEvent('category_updated', $category);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
