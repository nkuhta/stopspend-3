<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model {

    protected $fillable = ['inviter', 'invited', 'status'];
    protected $casts = [
        'inviter' => 'integer',
        'invited' => 'integer',
    ];

    public function invited()
    {
        return $this->belongsTo('App\User', 'invited', 'id')->first();
    }

    public function inviter()
    {
        return $this->belongsTo('App\User', 'inviter', 'id')->first();
    }

    public function perform()
    {
        $user = $this->invited();
        $inviter = $this->inviter();
        $user->setSpace($inviter->getSpace()->id);
        $this->status = 'accepted';
        $this->save();
    }

    public function cancel()
    {
        $this->status = 'cancelled';
        $this->save();
    }
}