<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model {
    protected $fillable = ['category_id', 'month_id', 'amount', 'author_id'];
    protected $appends = ['category_name'];
    protected $casts = [
        'month_id' => 'integer',
        'author_id' => 'integer',
        'amount' => 'float',
        'category_id' => 'integer'
    ];

    /**
     * @return \App\Model\Category
     */
    public function category() {
        return $this->belongsTo('App\Model\Category')->first();
    }

    /**
     * @return \App\Model\Month
     */
    public function month() {
        return $this->belongsTo('App\Model\Month')->first();
    }

    public function getCategoryNameAttribute()
    {
        return $this->category()->name;
    }

    public function setCategoryNameAttribute($value)
    {
        $category = $this->month()->space()->getCategory($value);
        $this->category_id = $category->id;
    }

    public function belongsToUser($userId = null) {
        return $this->month()->belongsToUser($userId);
    }

}