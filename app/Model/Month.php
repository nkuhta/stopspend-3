<?php

namespace App\Model;

use App\Library\Calculator;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Month extends Model {
    protected $fillable = ['date', 'author_id', 'space_id'];
    protected $casts = [
        'space_id' => 'integer',
        'author_id' => 'integer',
        'income' => 'float',
        'plan_copied' => 'integer'
    ];
    protected $calculator;

    public function isCurrentMonth()
    {
        return $this->date === date('Y-m');
    }

    public function isFutureMonth()
    {
        $month = date('Ym', strtotime($this->date . '-01'));
        return date('Ym') < $month;
    }

    public function isPastMonth()
    {
        $month = date('Ym', strtotime($this->date . '-01'));
        return date('Ym') > $month;
    }

    public function emitSocketEvent($event, $data) {
        \App\Events\Socket::emit('month' . $this->id, $event, $data);
    }

    public function belongsToUser($user_id = null) {
        if ($user_id === null && \Auth::check()) {
            $user_id = \Auth::user()->id;
        }
        $user = User::find($user_id);
        if (!$user) {
            throw new \Exception('User not found for check belong');
        }
        $usersSpace = $user->getSpace();

        return $this->space()->id === $usersSpace->id || $this->author_id === $user_id;
    }

    public function space() {
        return $this->belongsTo('App\Model\Space', 'space_id')->first();
    }

    public function getState() {
        return [
            'income' => $this->income,
            'date' => $this->date . '-01'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function operations() {
        return $this->hasMany('App\Model\Operation');
    }

    public function plans() {
        return $this->hasMany('App\Model\Plan');
    }
    
    public function getCalculator()
    {
        if (!$this->calculator) {
            $this->calculator = new Calculator($this);
        }
        return $this->calculator;
    }

    /**
     * Пока только заглушка
     * @return int
     */
    public function getHealth()
    {
        return $this->getCalculator()->getHealth();
    }

    public function getBalance()
    {
        $calculator = $this->getCalculator();
        return $calculator->getMonthBalance();
    }

    public function getTitle()
    {
        $names = [
            '',
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь'
        ];
        $date = $this->date;
        $month = (int)date('m', strtotime($date . '-01'));
        $monthName = $names[$month];
        return $monthName . ' ' . date('Y', strtotime($date . '-01'));
    }

    public function getPreviousMonth()
    {
        $currentDate = new \DateTime($this->date . '-01');
        $previousMonthDate = $currentDate->sub(new \DateInterval('P01M'));
        return Month::where('date', $previousMonthDate->format('Y-m'))->first();
    }

    public function getInheritedBalance()
    {
        $previousMonth = $this->getPreviousMonth();
        if (!$previousMonth) {
            return 0;
        }
        return $previousMonth->getBalance();
    }
}