<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model {
    protected $fillable = ['category_id', 'dt', 'month_id', 'amount', 'author_id', 'description', 'is_credit'];
    protected $appends = ['category_name', 'author_avatar'];
    protected $casts = [
        'is_credit' => 'boolean',
        'month_id' => 'integer',
        'amount' => 'float',
        'author_id' => 'integer',
        'category_id' => 'integer'
    ];

    /**
     * @return \App\Model\Category
     */
    public function category() {
        return $this->belongsTo('App\Model\Category')->first();
    }

    /**
     * @return \App\Model\Month
     */
    public function month() {
        return $this->belongsTo('App\Model\Month')->first();
    }

    public function author() {
        return $this->belongsTo('App\User')->first();
    }

    public function getCategoryNameAttribute()
    {
        return $this->category()->name;
    }

    public function setCategoryNameAttribute($value)
    {
        $category = $this->month()->space()->getCategory($value);
        $this->category_id = $category->id;
    }

    public function getAuthorAvatarAttribute()
    {
        return $this->author()->avatar;
    }

    public function belongsToUser($userId = null) {
        return $this->month()->belongsToUser($userId);
    }

}