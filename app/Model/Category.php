<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Category extends Model {
    protected $fillable = ['name', 'space_id'];
    protected $casts = [
        'space_id' => 'integer'
    ];
    protected $appends = ['count_of_use'];

    public function space() {
        return $this->belongsTo('App\Model\Space')->first();
    }

    public function operations() {
        return $this->hasMany('App\Model\Operation');
    }

    public function plans() {
        return $this->hasMany('App\Model\Plan');
    }

    public function belongsToUser($userId = null)
    {
        return $this->space()->belongsToUser($userId);
    }

    /**
     * Категория связана с планом или с операцией ?
     */
    public function linkedWithSomething()
    {
        return ($this->operations()->count() > 0 || $this->plans()->count() > 0);
    }

    public function getCountOfUseAttribute()
    {
        $operations = $this->hasMany('App\Model\Operation');
        return $operations->count();
    }

}