<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Space extends Model {

    protected $fillable = ['author_id'];
    protected $casts = [
        'author_id' => 'integer'
    ];

    public function emitSocketEvent($event, $data)
    {
        \App\Events\Socket::emit('space' . $this->id, $event, $data);
    }

    public function getMonths()
    {
        return $this->hasMany('App\Model\Month');
    }

    public function month()
    {
        return $this->hasMany('App\Model\Month');
    }

    public function author()
    {
        return $this->belongsTo('App\User')->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany('App\Model\Category');
    }

    public function belongsToUser($userId = null)
    {
        if (!$userId) {
            $userId = \Auth::user()->id;
        }
        if ($this->author_id === $userId || $this->id === \Auth::user()->space_id) {
            return true;
        }
        return false;
    }

    /**
     * @param $name
     * @return Category
     */
    public function getCategory($name)
    {
        $category = $this->categories()->where('name', $name)->first();
        if (!$category) {
            $category = new Category([
                'name' => $name,
                'space_id' => $this->id
            ]);
            $category->save();
        }
        return $category;
    }
}