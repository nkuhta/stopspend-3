<?php

namespace App\Exceptions;

// Если ошибка типа Error, то в консоль сыпаться ошибки на клиенте не будут. Считается, что это нормальный ход событий.
class Error extends \Exception
{}