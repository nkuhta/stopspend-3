define [
  'underscore',
  'moment',
  'collections/operations',
  'collections/plans',
  'collections/days'
  'common'
], (_, moment, Operations, Plans, Days, Common) ->
  Master = null
  class Calculator
    plans: {}
    plansSumm: 0
    days: {}
    daysCount: 0
    outcomes: {} # Расходы на каждый день (но только неплановые расходы)
    totalOutcome: 0


    constructor: ->
      @plans = {}
      @plansSumm = 0
      @days = {}
      @daysCount = 0
      @outcomes = {} # Расходы на каждый день (но только неплановые расходы)
      @totalOutcome = 0
      @health = 0

    initializePlans: ->
      # Берем все планы, по идентификатору категории загоняем в хеш,
      # из которого будет потом вычитать при переборе операций
      @plans = {}
      Plans.each (plan) =>
        @plans[plan.get 'category_id'] = 0 unless @plans[plan.get 'category_id']
        amount = parseFloat plan.get 'amount'
        @plans[plan.get 'category_id'] += amount
        @plansSumm += amount

    initializeDays: ->
      start = state.month.clone().startOf('month')
      end = start.clone().endOf('month')
      while start.isBefore(end)
        @days[start.format 'YYYY-MM-D'] = {
          spent: 0,
          balance: 0
        }
        @daysCount++
        start.add 1, 'day'

    countCanSpendPerDay: ->
      # Вычитаем из дохода все планы, делим на количество дней, вот вам можно тратить в день
      income = Master.get 'income'
      (income - @plansSumm) / @daysCount

    getPlanPartOfOperation: (operation) ->
      # Какая часть операции является фактической (вычитается плановая часть)
      category = operation.get 'category_id'
      amount = operation.get 'amount' # сумма у операции
      plan = @plans[category] or 0 # плановая сумма у этой категории
      result = 0

      if plan is 0 # плана вообще нет
        result = 0 # не может быть никакого планового факта, где нет плана вообще
      else if amount <= plan # если факт меньше плана
        result = amount # эта операция полность плановая
      else # если факт больше плана
        result = plan # лишь такая часть из этой операции является планом

      @plans[category] -= result if @plans.hasOwnProperty category # вычитаем из плана плановую часть операции
      result

    countOperations: ->
      # Идем по каждой операции, рассчитываем, плановая ли она или фактическая
      Operations.each (operation) =>
        planPart = @getPlanPartOfOperation operation
        operation.set
          'plan_part': planPart
        , silent: true
        return if operation.get 'is_credit' # Кредитные операции не влияют на баланс
        @totalOutcome += parseFloat operation.get 'amount'
        # У каждой операции теперь есть значение её плановой части
        # найдем день операции и добавим туда расход
        date = moment(operation.get 'dt').format 'YYYY-MM-D'
        day = @days[date]
        factPart = parseFloat(operation.get('amount')) - planPart
        day.spent += factPart

    countDaysBalance: ->
      lastBalance = 0
      firstDay = true
      for dt, day of @days
        # Добавляем наследуемый из прошлого песяца баланс
        day.balance += state.inheritedBalance if firstDay and state.inheritedBalance
        firstDay = false

        day.initialBalance = day.balance += lastBalance + @canSpendPerDay

        day.balance -= day.spent
        lastBalance = day.balance

    updatePlans: ->
      # Проходим по всем операциям и вытаскиваем из них, насколько по ФАКТУ какая категория выполнена
      # затем идем по категориям и им заносим эти значения в факт
      facts = {}
      Operations.each (operation) ->
        categoryId = operation.get 'category_id'
        facts[categoryId] = 0 unless facts[categoryId]
        facts[categoryId] += operation.get 'amount'

      plansHash = {}

      Plans.each (plan) ->
        categoryId = plan.get 'category_id'
        plansHash[categoryId] = [] unless plansHash[categoryId]
        plansHash[categoryId].push plan

      for categoryId, plans of plansHash
        spent = facts[categoryId] or 0
        for plan, i in plans
          last = i is plans.length - 1
          amount = plan.get 'amount'
          if amount > spent # по факту затрачено меньше, чем по плану
            plan.set
              spent: spent
            , silent: true
            spent = 0
          else # по факту затрачено больше, чем по плану
            if last # это последний член набора, он получает всю сумму spent на себя
              plan.set
                spent: spent
              , silent: true
              spent = 0
            else
              plan.set
                spent: amount
              , silent: true
              spent -= amount

    isCurrentMonth: -> Common.isCurrentMonth()
    isPastMonth: -> Common.isPastMonth()
    isFutureMonth: -> Common.isFutureMonth()

    countHealth: ->
      totalDays = 0
      positiveDays = 0
      if @isFutureMonth()
        @health = 0
      else
        today = moment()
        positiveDays = 0
        for date, day of @days
          if moment(date).isBefore(today)
            totalDays++
            positiveDays++ if day.balance > 0
        @health = Math.round ((positiveDays / totalDays) * 100)

    countCanSpendPerDayFuture: ->
      # Сколько всего осталось денег
      spent = Operations.reduce(
        (spent, r) ->
          (unless r.get('is_credit') then r.get('amount') - r.get('plan_part') else 0) + spent
        , 0
      )
      left = Master.get('income') - @plansSumm - spent
      if @isCurrentMonth()
        endOfMonth = moment(Master.get('date')).endOf('month')
        days = endOfMonth.diff(moment(), 'days')
        # Сколько можно тратить в день, чтобы месяц вышел в "ноль"
        @canSpendPerDayFuture = left / days
      else
        @canSpendPerDayFuture = 0

    calculate: (master) ->
      if Calculator.suspend is true
        return
      ###
        1) Считаем сколько можно тратить в день
        2) Идем по дням. Баланс дня = Баланс с предыдущего дня + баланс с текущего дня
      ###

      Master = master
      @initializePlans() # инициализируем значения планов
      @initializeDays() # инициализируем дни, в которые будет писаться баланс
      @canSpendPerDay = @countCanSpendPerDay()
      @countOperations() # Расчет фактических операций
      @countDaysBalance() # расчет балансов на каждый день
      @updatePlans() # плановым операциям проставляем, насколько они выполнены по факту
      @countCanSpendPerDayFuture() # Рассчитаем, сколько можно тратить в день, чтобы месяц вышел в ноль

      days = []

      todayBalance = 0
      todaySpent = 0
      todayInitialBalance = 0
      today = moment().format('YYYY-MM-D')
      for dt, day of @days
        days.push
          dt: dt
          balance: day.balance
          spent: day.spent
        todayBalance = day.balance if dt is today
        todayInitialBalance = day.initialBalance if dt is today # Сколько мог потратить сегодня вообще
        todaySpent = day.spent if dt is today
        monthBalance = day.balance


      @countHealth()

      Calculator.suspend = true
      Master.set
        can_spend_per_day: @canSpendPerDay # сколько можем тратить в день
        can_spend_per_day_future: @canSpendPerDayFuture # сколько можем тратить в день
        plan_expense: @plansSumm # общий плановый расход
        spent: @totalOutcome
        left: Master.get('income') - @totalOutcome,
        can_spend_today: todayBalance
        could_spend_today: todayInitialBalance
        spent_today: todaySpent
        health: @health
        monthBalance: monthBalance

      Operations.trigger 'reset'
      Plans.trigger 'reset'
      Days.reset days
      Calculator.suspend = false

  Calculator