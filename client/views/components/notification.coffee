define [
  'backbone'
], (Backbone) ->
  Backbone.View.extend(
    tag: 'div'
    className: 'alert alert-dismissible'

    events:
      'click a.alert-button[role=yes]': 'onYesClick'
      'click a.alert-button[role=no]': 'onNoClick'
      'click a.alert-button[role=not_now]': 'close'

    initialize: (config) ->
      _.extend this, config
      $('.notifications-container').append @render().el
      @type = 'info' unless @type
      @$el.addClass "alert-#{@type}"

    render: ->
      buttonsWidth = null
      message = $("<span></span>").html(@message)
      if @buttons is 'YES_NO' or @buttons is 'YES_NO_NOTNOW'
        btn = $('<div class="button-container"></div>')
        btn.append($('<a href="#" class="alert-link alert-button" role="yes">Да</a>'))
        btn.append($('<a href="#" class="alert-link alert-button" role="no">Нет</a>'))
        buttonsWidth = 85
        if @buttons is 'YES_NO_NOTNOW'
          btn.append($('<a href="#" class="alert-link alert-button" role="not_now">Не сейчас</a>'))
          buttonsWidth = 170
      else
        btn = $('\
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> \
          <span aria-hidden="true">&times;</span> \
          </button>'
        )
      btn.width(buttonsWidth) if buttonsWidth
      table = $('<table><tr><td></td><td></td></tr></table>')
      table.find('tr td:nth-child(1)').append message
      table.find('tr td:nth-child(2)').append btn
      @$el.append table
      this

    close: -> @remove()

    onYesClick: (e) ->
      e.preventDefault()
      @callback('yes') if @callback and _.isFunction(@callback)
      @close()

    onNoClick: (e) ->
      e.preventDefault()
      @callback('no') if @callback and _.isFunction(@callback)
      @close()

  )