define [
  'backbone'
  'selectize'
], (Backbone, Selectize) ->
  Backbone.View.extend(
    tag: 'div'
    className: 'input-group'
    initialize: (config) -> _.extend this, config
    render: ->
      select = $('<select></select>').attr('placeholder', @placeholder or '').attr('name', @name or '')
      @$el.width(@width) if @width
      @$el.append select
      @init()
      this

    init: ->
      options = []
      if @options
        options = @options
      else if @collection
        options = @collection.map (record) ->
          text: record.get 'name'
          value: record.get 'name'
      @$('select').selectize
        sortField: @sortField or []
        create: true,
        options: options
        items: if @value then [@value] else []
  )