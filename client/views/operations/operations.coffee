define [
  'backbone'
  'mustache'
  'moment'
  'views/base/collection'
  'text!templates/operations/operations.mustache'
  'collections/operations'
  'views/operations/operation'
  'views/base/modal'
  'views/operations/form'
], (Backbone, mustache, moment, BaseView, template, Operations, OperationView, Modal, OperationsForm) ->
  MasterView = BaseView.extend(
    template: template
    collection: Operations
    rowContainerSelector: 'tbody'
    itemView: OperationView
    getModal: ->
      new Modal
        contentView: OperationsForm
        title: 'Добавление операции'

    filter: (param) ->
      @filterType = param
      @renderRows()
      $('a[role=operation_sorting]').removeClass 'active'
      $("a[role=operation_sorting][sort=#{param}]").addClass 'active'

    getRows: ->
      yesterday = moment().startOf('day').add(-1, 'day')
      weekAgo = moment().startOf('day').add(-1, 'week')
      filter = @filterType
      @collection.filter (record) =>
        date = moment record.get('dt')
        if not filter or filter is 'all'
          true
        else if filter is 'yesterday'
          date.isAfter yesterday
        else if filter is 'week'
          date.isAfter weekAgo
  )
  MasterView
