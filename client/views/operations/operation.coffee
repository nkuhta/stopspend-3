define [
  'backbone'
  'text!templates/operations/operation.mustache'
  'api'
  'views/base/item'
  'collections/categories'
  'views/components/selectize'
  'common'
  'moment'
], (Backbone, template, Api, ItemView, Categories, Selectize, Common, moment) ->
  view = ItemView.extend(
    className: 'operation'
    template: template
    api:
      update: Api.updateOperation
      delete: Api.deleteOperation
    fields: ['dt', 'category_name', 'amount', 'description', 'is_credit']

    getConstraints: ->
      dt:
        presence: true
        datetime:
          format: 'D.MM.YYYY HH:mm'
          earliest: state.month.toDate()
          latest: state.month.clone().add(1, 'month').add(-1, 'second').toDate()
      category_name:
        presence: true
      amount:
        presence: true
        numericality: true

    _createInput: (key) ->
      if key is 'dt'
        input = $('<div class="input-group date"></div>')
        input.append(
          $('<input class="form-control"/>')
          .attr('type', 'text')
          .attr('name', key)
          .attr('required', true)
          .val(@model.get(key))
        )
        input.append $('<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>')
        input.datetimepicker(
          locale: 'ru'
          format: 'D.MM.YYYY HH:mm'
        )
        input.data('DateTimePicker').date(moment(@model.get(key)).toDate())
      else if key is 'category_name'
        input = (new Selectize
          name: key
          options: Categories.getOptionsSortedByUse()
          placeholder: 'Категория'
          width: '100%'
          value: @model.get('category_name')
          sortField: [
            field: 'use'
            direction: 'desc'
          ]
        ).render().el
      else if key is 'description'
        input = $ '<input class="form-control" placeholder="Описание" name="description"></input>'
        input.val @model.get 'description'
      else if key is 'is_credit'
        input = $ '<input type="checkbox"/>'
        input.attr('name', 'is_credit').attr('title', 'Кредитная операция (не влияет на баланс)')
        if @model.get('is_credit') then input.attr('checked', 'checked') else input.removeAttr('checked')
      else
        input = ItemView.prototype._createInput.apply(this, arguments)

      if key is 'amount'
        $(input).find('input').formular()

      input

    edit: ->
      ItemView.prototype.edit.apply this, arguments
      # В ячейку с категорией добавляем еще и описание
      container = @$('td[field=category_name]')
      textarea = @_createInput 'description'
      container.append textarea

    beforeRemove: (callback) ->
      Common.confirm 'Вы уверены?', 'Вы уверены, что хотите удалить операцию?', callback
  )
  view
