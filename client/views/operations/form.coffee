define [
  'backbone'
  'mustache'
  'underscore'
  'views/base/form'
  'api'
  'text!templates/operations/form.mustache'
  'datepicker'
  'views/components/selectize'
  'collections/categories'
  'moment'
  'parser'
  'collections/operations'
], (Backbone, mustache, _, Form, Api, template, Datepicker, Selectize, Categories, moment, Parser, Operations) ->
  Form.extend
    template: template
    collection: Operations
    fields: ['dt', 'category', 'amount']

    getConstraints: ->
      dt:
        presence: true
        datetime:
          format: 'D.MM.YYYY HH:mm'
          earliest: state.month.toDate()
          latest: state.month.clone().add(1, 'month').toDate()
      category:
        presence: true
      amount:
        presence: true
        numericality: true

    render: ->
      Form.prototype.render.call this
      @$('div.date').datetimepicker
        locale: 'ru'
        format: 'D.MM.YYYY HH:mm'

      selectize = (new Selectize
        options: Categories.getOptionsSortedByUse()
        placeholder: 'Категория'
        width: '100%'
        name: 'category'
        sortField: [
          field: 'use'
          direction: 'desc'
        ]
      ).render().el

      @$('input[name=amount]').formular()

      @$('input[name=category]').replaceWith selectize

      # Дата в форме должна быть подставлена автоматически
      # Если она не в этом месяце, то тогда подставляем крайние значения
      today = moment()
      if today.isBetween state.monthStart, state.monthEnd
        # сегодня в этом месяце
        date = today
      else if today.isBefore state.monthStart
        # выбран будущий месяц, надо подставить его первое число в поле
        date = state.monthStart
      else if today.isAfter state.monthEnd
        # выбран прошлый месяц, надо подставить его первое число в поле
        date = state.monthEnd
      @$('div.date').data('DateTimePicker').date(new Date())
      this

    onSave: (data, callback) ->
      Api.addOperation data, callback
