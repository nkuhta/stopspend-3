define [
  'backbone'
  'mustache'
  'common',
  'validate'
], (Backbone, mustache, Common, validate) ->
  View = Backbone.View.extend
    tagName: 'tr'
    events:
      'click a[role=edit]': 'edit'
      'click a[role=save]': 'save'
      'click a[role=remove]': 'onRemoveClick'
      'keypress input': 'onInputKeyPress'

    initialize: (config) ->
      _.extend this, config
      @listenTo @model, 'edit', @edit
      @listenTo @model, 'change', @render
      @listenTo @model, 'remove destroy', @remove

    getData: -> @model.toJSON()

    render: ->
      @$el.html mustache.to_html @template, @getData()
      this

    _createInput: (key) ->
      input = $('<input class="form-control"/>').attr('type', 'text').attr('name', key).val(@model.get(key))
      container = $ '<div class="input-group"></div>'
      container.append input

    edit: (e) ->
      e.preventDefault() if e
      # Прячем содержимое ячеек Даты, Категории и Суммы, а вместо них показываем input
      _.each @fields, (key) =>
        container = @$('td[field=' + key + ']')
        return unless container
        input = @_createInput key
        container.empty().append input

      @$('input[name=' + @fields[0] + ']').focus()
      @$('a[role=edit]').hide()
      @$('a[role=save]').show()

    onRemoveClick: (e) ->
      e.preventDefault()
      if @beforeRemove
        @beforeRemove (confirmed) =>
          if confirmed
            @api.delete @model.id, (success) =>
              @collectionView.collection.remove(@model.id) if (success)
      else
        @api.delete @model.id, (success) =>
          @collectionView.collection.deleteIfExists(@model.id) if (success)

    validateEdit: (data) ->
      constraints = if _.isFunction(@getConstraints) then @getConstraints() else @constraints
      throw new Error('Constratins not defined') unless constraints
      value = validate data, constraints
      if value
        # для каждого поля добавляем класс с ошибкой
        for key of value
          @$("input[name=#{key}],select[name=#{key}]").parent().addClass 'has-error'
      return not value

    onInputKeyPress: (e) ->
      if e.which == Common.ENTER_KEY
        @save()

    save: (e) ->
      e.preventDefault() if e
      obj = {}
      _this = this
      @$('input, select').each (i, input) ->
        name = $(input).attr('name')
        obj[name] = $(input).val() if name
        if $(input).attr('type') is 'checkbox'
          obj[name] = $(input).is(':checked') # Конвертация в boolean
        return # чтобы не вернуть false и не прервать цикл
      return unless @validateEdit(obj) # не валидная форма
      if @model.isNew()
        _this.model.destroy()
        @api.add obj
      else
        @api.update @model.get('id'), obj, (data) =>
          @onSaved(data)
      @render()

    onSaved: (data) ->
      @collectionView.collection.createOrUpdate data
  View
