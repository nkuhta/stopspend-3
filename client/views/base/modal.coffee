define [
  'backbone'
  'mustache'
  'bootstrap'
  'text!templates/base/modal.mustache'
], (Backbone, mustache, bootstrap, template) ->
  MasterView = Backbone.View.extend(
    className: 'modal fade'
    template: template
    events:
      'click button[role=save]': 'saveForm'
      'click button[role=close]': 'close'

    initialize: (options) ->
      _.extend this, options
      @form = new (@contentView)(modal: this)

    render: ->
      @$el.html mustache.to_html @template,
        title: @title or ''
      @$('.modal-body').append @form.render().el
      this

    open: ->
      if !@isRendered
        @render()
      @$el.modal()

    saveForm: ->
      @form.save()

    close: ->
      @form.remove()
      @$el.modal 'hide'
      _this = this
      @$el.on 'hidden.bs.modal', (e) ->
        _this.remove()
  )
  MasterView
