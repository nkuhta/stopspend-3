define [
  'backbone'
  'mustache'
  'api'
], (Backbone, mustache, template, Api) ->
  Backbone.View.extend(
    tagName: 'form'
    initialize: (config) ->
      _.extend this, config
    render: ->
      @$el.html mustache.to_html(@template)
      this
    getData: ->
      data = @$el.serializeHash()
      @$('input[type=checkbox]').each (i, checkbox) ->
        # Для чекбоксов переопределяем стандартное поведение
        name = $(checkbox).attr('name')
        data[name] = $(checkbox).is(':checked') if name
      data
    validate: (data) ->
      constraints = if _.isFunction(@getConstraints) then @getConstraints() else @constraints
      throw new Error('Constratins not defined') unless constraints
      value = validate data, constraints
      for field in @fields
        container = @$("input[name=#{field}],select[name=#{field}]").parent()
        if value and value[field]
          container.addClass 'has-error'
        else
          container.removeClass 'has-error'
      return not value
    save: (callback) ->
      data = @getData()
      return unless @validate(data) # валидная ли форма?
      @onSave data, (data) =>
        @collection.createOrUpdate(data)
        @modal.close()

  )
