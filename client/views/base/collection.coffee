define [
  'backbone'
  'mustache'
], (Backbone, Mustache) ->
  BaseView = Backbone.View.extend(
    tag: 'div'

    initialize: (config) ->
      _.extend this, config
      @events = {} unless @events
      _.extend @events,
        'click .add-button': 'onAddClick'
      @listenTo @collection, 'add reset remove', @renderRows

    render: ->
      @$el.html Mustache.to_html(@template)
      this

    onAddClick: ->
      if _.isFunction(@getModal)
        modal = @getModal()
        modal.open()
      else
        console.warn 'getModal method not defined'

    getRows: ->
      @collection.map (r) -> r

    renderRows: ->
      _this = this
      if @collection and @rowContainerSelector
        container = @$(@rowContainerSelector)
        container.empty()
        for row in @getRows()
          view = new (_this.itemView)(
            model: row
            collectionView: this
          )
          container.append view.render().el
  )
  BaseView
