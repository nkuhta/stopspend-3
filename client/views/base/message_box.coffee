define [
  'backbone'
  'text!templates/base/message_box.mustache'
  'mustache'
], (Backbone, template, mustache) ->
  Backbone.View.extend
    className: 'modal fade'
    template: template
    events:
      'click button[role=ok]': 'ok'
      'click button[role=yes]': 'yes'
      'click button[role=no]': 'ok'

    initialize: (options) ->
      _.extend this, options
      @render()

    render: ->
      @$el.html mustache.to_html @template,
        message: @message
        title: @title
      footer = @$('.modal-footer')
      if @type is 'alert'
        footer.append @_createButton('ok')
      else if @type is 'prompt'
        footer.append @_createButton('yes')
        footer.append @_createButton('no')
      @$el.modal()
      this

    _createButton: (type) ->
      button = $('<button></button>')
      switch type
        when 'ok' then label = 'Ок'
        when 'yes' then label = 'Да'
        when 'no' then label = 'Нет'
      button.attr('type', 'button').attr('role', type).addClass('btn btn-default').html(label)
      button

    ok: ->
      @callback() if @callback
      @close()

    yes: ->
      @callback(true) if @callback
      @close()

    close: ->
      @$el.modal 'hide'
      @$el.on 'hidden.bs.modal', (e) =>
        @remove()

