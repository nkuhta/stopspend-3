define [
  'backbone'
  'mustache'
  'text!templates/help/window.mustache'
  'text!templates/help/menu.mustache'
], (Backbone, mustache, template, menuTemplate) ->
  Backbone.View.extend(
    className: 'modal fade'
    template: template
    events:
      'click button[role=close]': 'close'
      'click .section-selector': 'selectSection'

    initialize: (options) -> _.extend this, options

    render: ->
      @$el.html mustache.to_html @template,
        menu: mustache.to_html menuTemplate
      this

    open: ->
      if !@isRendered
        @render()
      @$el.modal()

    close: ->
      @$el.modal 'hide'
      @$el.on 'hidden.bs.modal', =>
        @remove()


    selectSection: (e) ->
      e.preventDefault()
      section = $(e.target).attr('section')
      @$('.section-selector').removeClass 'active'
      $(e.target).addClass 'active'
      url = "text!templates/help/sections/#{section}.mustache"
      require [url], (template) =>
        @$('.section-container').html mustache.to_html(template)
  )
