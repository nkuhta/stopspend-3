define [
  'backbone'
  'mustache'
  'text!templates/master.mustache'
  'models/master'
  'api'
  'underscore'
  'common'
  'sockets',
  'moment'
  'collections/days'
], (Backbone, mustache, template, MasterModel, Api, _, Common, Sockets, moment, Days) ->
  MasterView = Backbone.View.extend(
    tag: 'div'
    template: template
    events:
      'dblclick p.income span.summ': 'onSummDblClick'
      'keypress input': 'onInputKeyPress'
      'click a.previous-month-link': 'previousMonth'
      'click a.next-month-link': 'nextMonth'
      'click a[role=edit-income]': 'onSummDblClick'
      'click a[role=save-income]': 'finishEdit'
      'click div.control-button a[role=health]': 'showHealth'
      'click div.control-button a[role=money]': 'showMoney'
    model: MasterModel

    initialize: ->
      _this = this
      # При изменении модели всё перерисовывается
      @listenTo @model, 'change', @render
      @load()
      @listenTo Sockets, 'updated_master', (data) ->
        _this.model.set data

    render: ->
      data = @model.toJSON()
      data.month_caption = Common.ucfirst(state.month.format('MMMM YYYY'));
      data.isPastMonth = Common.isPastMonth()
      data.isCurrentMonth = Common.isCurrentMonth()
      if data.isCurrentMonth
        data.today_is_negative = data.can_spend_today < 0
        data.date = moment().format('D MMMM YYYY')
      else if data.isPastMonth
        data.month_balance = @model.get('monthBalance') or 0
        data.month_balance_class = 'red'
        data.month_balance_class = 'green' if data.month_balance >= 0
      data.health_class = 'red'
      data.health_class = 'yellow' if data.health >= 50 and data.health < 80
      data.health_class = 'green' if data.health >= 80

      @$el.html mustache.to_html(@template, data)
      @renderCircle()
      @$('p.income span.with_tooltip').tooltip
        placement: 'bottom'
        title: 'Ваш месячный доход. Если вы не знаете его точно, введите примерно, \
          а потом скорректируете, когда будет известно точнее.'
      @$('p.plan span.with_tooltip').tooltip
        title: 'Сумма плановых расходов в этом месяце'
      @$('p.spent span.with_tooltip').tooltip
        title: 'Сколько уже потрачено'
      @$('p.left span.with_tooltip').tooltip
        title: 'Оставшиеся средства'
      @$('p.can_spend_per_day span.with_tooltip').tooltip
        title: 'Столько вы можете тратить в день, чтобы ваш баланс в конце месяца был нулевым'
      @$('h1.red.with_tooltip').tooltip
        title: 'Вы тратили слишком много и ваш баланс ниже нуля. Вам придется подождать, пока он не восстановится.'
      @$('h1.green.with_tooltip').tooltip
        title: 'У вас положительный баланс, и вы еще можете тратить.'

      this

    load: ->
      _this = this
      Api.getMasterData (data) ->
        _.mapObject data, (value, key) ->
          _this.model.set key, value

    onSummDblClick: (e) ->
      # Редактируем
      span = @$('p.income span.summ')
      p = span.closest('p')
      type = p.attr('class')
      span.hide()
      input = span.next('input')
      input.show().val(@model.get('income')).select()
      @$('a[role=edit-income]').hide()
      @$('a[role=save-income]').show()
      # Добавляем input для ввода суммы после элемента отображения суммы

    onInputKeyPress: (e) ->
      if e.which == Common.ENTER_KEY
        @finishEdit()

    finishEdit: ->
      input = @$('input.summ-editor')
      value = parseFloat(input.val().trim())
      if _.isNaN(value)
        return
        # не валидное значение
      Api.saveIncome value
      input.hide()
      @$('a[role=edit-income]').show()
      @$('a[role=save-income]').hide()
      @$('p.income span.summ').val(value).show()

    renderCircle: ->
      canvas = @$('canvas')
      ctx = canvas[0].getContext('2d')
      width = canvas.width()
      height = canvas.height()
      if !width or !height
        return
      # рассчитаем процент
      couldSpendToday = @model.get('could_spend_today')
      spentToday = @model.get('spent_today')
      balance = @model.get('can_spend_today')
      percentage = undefined
      if spentToday < 0
        percentage = 0
      else if couldSpendToday < 0
        percentage = 100
      else
        percentage = if couldSpendToday != 0 then (spentToday / couldSpendToday) * 100 else 100
        if percentage > 100
          percentage = 100
      thinLineWidth = 16
      thickLineWidth = thinLineWidth / 2
      ctx.lineWidth = thickLineWidth
      ctx.beginPath()
      ctx.strokeStyle = '#fff'
      ctx.arc width / 2, height / 2, width / 2 - (ctx.lineWidth) + thickLineWidth / 2, 0, Math.PI * 2
      ctx.stroke()
      ctx.lineWidth = thinLineWidth
      ctx.beginPath()
      ctx.strokeStyle = '#acacac'
      ctx.arc width / 2, height / 2, width / 2 - (ctx.lineWidth), 0, Math.PI * 2
      ctx.stroke()
      ctx.beginPath()
      ctx.strokeStyle = if balance < 0 then '#D36969' else '#8cb951'
      begin = Math.PI / 2
      end = begin + Math.PI * 2 * percentage / 100
      ctx.arc width / 2, height / 2, width / 2 - (ctx.lineWidth), begin, end
      ctx.stroke()

    previousMonth: (e) ->
      e.preventDefault()
      date = state.month.clone().add(-1, 'month')
      url = state.baseUrl + '/month/' + date.format('YYYY-MM')
      window.location.href = url

    nextMonth: (e) ->
      e.preventDefault()
      date = state.month.clone().add(1, 'month')
      url = state.baseUrl + '/month/' + date.format('YYYY-MM')
      window.location.href = url

    showHealth: (e) ->
      e.preventDefault()
      @$('div.control-button a[role=health]').hide()
      @$('.money-information').hide()
      @$('div.control-button a[role=money]').show()
      @$('.health-information').show()

    showMoney: (e) ->
      e.preventDefault()
      @$('div.control-button a[role=health]').show()
      @$('.money-information').show()
      @$('div.control-button a[role=money]').hide()
      @$('.health-information').hide()

  )
  MasterView
