define [
  'backbone'
  'jquery'
  'underscore'
  'mustache'
  'text!templates/main.mustache'
  'views/master'
  'views/operations/operations'
  'views/plans/plans'
  'views/calendar/calendar'
  'views/categories/categories'
  'views/help'
  'views/reports/reports'
], (Backbone, $, _, mustache, mainTemplate, MasterView, OperationsView, PlansView, CalendarView, CategoriesView, Help, ReportsView) ->
  MainView = Backbone.View.extend(
    el: 'body'
    template: mainTemplate
    events:
      'click .tab-link': 'onTabLinkClick'
      'click a[role=help-btn]': 'onHelpClick'
    initialize: ->
      @masterView = new MasterView
      @operationsView = new OperationsView
      @plansView = new PlansView
      @calendarView = new CalendarView
      @categoriesView = new CategoriesView
      @reportsView = new ReportsView
      return
    render: ->
      @$el.html mustache.to_html @template,
        base_url: state.baseUrl
      @$('section.master').append @masterView.render().el
      @$('section.operations').append @operationsView.render().el
      @$('section.plans').append @plansView.render().el
      @$('section.calendar').append @calendarView.render().el
      @$('section.categories').append @categoriesView.render().el
      @$('section.reports').append @reportsView.render().el
      this

    onTabLinkClick: (e) ->
      e.preventDefault()
      tab = $(e.target).attr('tab')
      window.location.hash = '#' + tab

    selectTab: (tab, params) ->
      @$el.find('section.tab').hide()
      @$el.find('section.tab.' + tab).show()
      @$('.tab-link').removeClass 'active'
      @$('.tab-link[tab=' + tab + ']').addClass 'active'

      if (tab is 'operations' and params and params.filter)
        @operationsView.filter params.filter
      else if (tab is 'reports' and params and params.type)
        @reportsView.setChart params.type


    onHelpClick: ->
      help = new Help()
      help.open()

  )
  new MainView()
