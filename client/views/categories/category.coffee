define [
  'backbone'
  'views/base/item'
  'text!templates/categories/category.mustache'
  'api'
  'common'
], (Backbone, ItemView, template, Api, Common) ->
  ItemView.extend
    className: 'category'
    template: template
    api:
      add: Api.addCategory
      update: Api.updateCategory
      delete: Api.deleteCategory
    fields: ['name']
    constraints:
      name:
        presence: true

    beforeRemove: (callback) ->
      Common.confirm 'Вы уверены?', 'Вы уверены, что хотите удалить категорию?', callback