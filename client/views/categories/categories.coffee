define [
  'backbone'
  'views/base/collection'
  'text!templates/categories/categories.mustache'
  'collections/categories'
  'views/categories/category'
], (Backbone, CollectionView, template, Categories, CategoryView) ->
  CollectionView.extend
    template: template
    collection: Categories
    rowContainerSelector: 'tbody'
    itemView: CategoryView
    onAddClick: ->
      Categories.add(name: '').last().trigger 'edit'
