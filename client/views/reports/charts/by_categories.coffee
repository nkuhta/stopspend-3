define [
  'backbone'
  'underscore'
  'views/reports/charts/base'
  'chart'
  'api'
], (Backbone, _, BaseView, Chart, Api) ->
  BaseView.extend
    loadData: ->

      getRandomColor = () ->
        color = ''
        while (!color.match(/(#[c-e].)([e-f][a-f])([9-c].)/))
          color = '#' + Math.floor(Math.random() * (Math.pow(16,6))).toString(16)
        color

      Api.getReport 'by_categories', @getFilterData(), (categories) =>
        labels = []
        data = []
        colors = []
        _.forEach categories, (category) ->
          labels.push category.name
          data.push category.summ
          colors.push getRandomColor()

        @chart = new Chart @el,
          type: 'pie'
          data: {
            labels: labels
            datasets: [
              data: data
              backgroundColor: colors
            ]
          }

