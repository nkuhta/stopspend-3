define [
  'backbone'
  'underscore'
], (Backbone, _) ->
  Backbone.View.extend
    tagName: 'canvas'
    initialize: (config) ->
      _.extend(this, config)
    getFilterData: ->
      @filter.getState()
    render: ->
      this