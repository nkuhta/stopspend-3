define [
  'backbone'
  'mustache'
  'text!templates/reports/filter.mustache'
], (Backbone, Mustache, template) ->
  Backbone.View.extend(
    tag: 'div'
    template: template
    events:
      'click button': 'setMonth'

    render: ->
      @$el.html Mustache.to_html(@template)
      this

    setMonth: (e) ->
      console.log 'setmonth'
      link = $(e.target)
      @$('button').removeClass('active')
      link.addClass('active')
      @trigger('filterChanged')

    getState: ->
      month: @$('button.active').attr('data-type')
  )