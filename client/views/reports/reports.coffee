define [
  'backbone'
  'mustache'
  'text!templates/reports/main.mustache'
  'views/reports/charts/by_categories'
  'views/reports/filter'
], (Backbone, Mustache, template, CategoriesChart, FilterView) ->
  charts =
    by_categories: CategoriesChart
  Backbone.View.extend(
    tag: 'div'
    template: template
    initialize: ->
      @filterView = new FilterView()
      @filterView.render()
      @listenTo @filterView, 'filterChanged', @reloadChart

    events:
      'click .list-group a': 'clickLink'

    render: ->
      @$el.html Mustache.to_html(@template)
      @$('.filter-area').append(@filterView.el)

      this

    clickLink: (e) ->
      type = $(e.target).attr 'chart-type'
      location = "reports/#{type}"
      window.location.hash = location
      e.preventDefault()

    setChart: (type) ->
      @chartType = type
      @reloadChart()

    reloadChart: ->
      chart = charts[@chartType]
      chart = new chart(
        filter: @filterView
      )
      @$('.chart-area').empty().append(chart.render().el)
      chart.loadData()
  )
