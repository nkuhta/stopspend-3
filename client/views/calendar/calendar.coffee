define [
  'backbone'
  'mustache'
  'underscore'
  'moment'
  'text!templates/calendar/calendar.mustache'
  'collections/days'
  'models/master'
], (Backbone, Mustache, _, moment, template, Days, Master) ->
  Backbone.View.extend
    tag: 'div'
    template: template

    initialize: ->
      @listenTo Days, 'reset', @render

    getWeeksForTemplate: ->
      weeks = {}
      monthNum = state.month.month()
      lastDay = state.month.clone().endOf('month').endOf('week')
      current = state.month.clone().startOf('week')
      # Переходим к первому дню недели
      while current < lastDay
        dayRecord = Days.findWhere(dt: current.format('YYYY-MM-D'))
        balance = if dayRecord and dayRecord.get('balance') then dayRecord.get('balance') else 0
        spent = if dayRecord and dayRecord.get('spent') then dayRecord.get('spent') else 0
        can_spend_per_day = Master.get 'can_spend_per_day'
        day =
          empty: current.month() != monthNum
          dt: current.format('ddd D MMMM')
          balance: balance
          spent: spent
          can_spend: can_spend_per_day
          spent_cls: if spent < can_spend_per_day then 'green' else 'red'

        if state.inheritedBalance # Есть унаследованный баланс, надо вывести его в календаре
          nextDay = current.clone().add 1, 'day'
          if nextDay.month() is monthNum
            day.inheritedBalance = state.inheritedBalance
            if day.inheritedBalance > 0
              day.inheritedBalanceCls = 'green'
            else if day.inheritedBalance < 0
              day.inheritedBalanceCls = 'red'
            else
              day.inheritedBalanceCls = 'grey'

        if balance >= 10000 or balance <= -1000
          day.small = true
          # Значение слишком длинное, надо уменьшить размер шрифта
        day.cls = if balance < 0 then 'red' else 'green'
        day.spent_cls = '' if current > moment() # У будущих дней сумма не подсвечивается
        # Если день в будущем и он зеленый, то надо сделать его серым (будто еще неизвестно)
        if current > moment() and day.cls == 'green'
          day.cls = 'grey'
        if balance == 0
          day.balance = '0'
          # Чтоб в mustache срабатывало true при проверке
        weekNumber = current.format('w')
        if !weeks[weekNumber]
          weeks[weekNumber] = []
        weeks[weekNumber].push day
        current.add 1, 'day'
      _.values weeks
    render: ->
      @$el.html Mustache.to_html(@template, weeks: @getWeeksForTemplate())
      this
