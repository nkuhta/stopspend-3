define [
  'backbone'
  'mustache'
  'views/base/collection'
  'views/base/modal'
  'text!templates/plans/plans.mustache'
  'collections/plans'
  'views/plans/plan'
  'views/plans/form'
  'api'
  'common'
], (Backbone, mustache, BaseView, Modal, template, Plans, PlanView, PlansForm, Api, Common) ->
  MasterView = BaseView.extend(
    template: template
    collection: Plans
    rowContainerSelector: 'tbody'
    itemView: PlanView
    events:
      'click button[role=copy-plan-from-prev-month]': 'copyPlanFromPreviousMonth'
    getModal: ->
      new Modal
        contentView: PlansForm
        title: 'Добавление планового расхода'

    copyPlanFromPreviousMonth: ->
      Common.confirm 'Вы уверены?', 'Вы уверены, что хотите загрузить плановые операции из прошлого месяце? \
        Это удалит все текущие планы.', (confirmed) ->
        Api.copyPlansFromPreviousMonth() if confirmed

  )
  MasterView
