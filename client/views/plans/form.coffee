define [
  'backbone'
  'mustache'
  'views/base/form'
  'api'
  'text!templates/plans/form.mustache'
  'collections/categories'
  'collections/plans'
  'views/components/selectize'
], (Backbone, mustache, Form, Api, template, Categories, Plans, Selectize) ->
  Form.extend
    template: template
    collection: Plans
    fields: ['category', 'amount']
    constraints:
      category:
        presence: true
      amount:
        presence: true
        numericality: true

    onSave: (data, callback) ->
      Api.addPlan data, callback

    render: ->
      Form.prototype.render.call this
      selectize = (new Selectize
        name: 'category'
        options: Categories.getOptionsSortedByUse()
        placeholder: 'Категория'
        width: '100%'
        sortField: [
          field: 'use'
          direction: 'desc'
        ]
      ).render().el

      @$('input[name=amount]').formular()

      @$('input[name=category]').replaceWith selectize
      this
