define [
  'backbone'
  'mustache'
  'text!templates/plans/plan.mustache'
  'api'
  'views/base/item'
  'views/components/selectize'
  'collections/categories'
  'common'
], (Backbone, mustache, template, Api, ItemView, Selectize, Categories, Common) ->
  ItemView.extend(
    className: 'plan'
    template: template
    api:
      add: Api.addPlan
      update: Api.updatePlan
      delete: Api.deletePlan
    fields: ['category_name', 'amount']
    constraints:
      category_name:
        presence: true
      amount:
        presence: true
        numericality: true

    getData: ->
      data = ItemView.prototype.getData.apply(this, arguments)
      data.percentage_width = if data.percentage > 100 then 100 else data.percentage
      data.progress_bar_class = if data.percentage > 100 then 'red' else 'green'
      data

    _createInput: (key) ->
      if key is 'category_name'
        input = (new Selectize
          name: key
          options: Categories.getOptionsSortedByUse()
          placeholder: 'Категория'
          width: '100%'
          value: @model.get('category_name')
          sortField: [
            field: 'use'
            direction: 'desc'
          ]
        ).render().el
      else
        input = ItemView.prototype._createInput.apply(this, arguments)

      if key is 'amount'
        $(input).find('input').formular()

      input

    beforeRemove: (callback) ->
      Common.confirm 'Вы уверены?', 'Вы уверены, что хотите удалить плановый расход?', callback
  )
