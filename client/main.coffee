require.config
  shim:
    'bootstrap': 'deps': [ 'jquery' ]
    underscore: exports: '_'
    backbone:
      deps: [
        'underscore'
        'jquery'
      ]
      exports: 'Backbone'
    parser: exports: 'Parser'
    chart: exports: 'Chart'
  paths:
    bootstrap: '../bootstrap/js/bootstrap'
    backbone: '../js/backbone'
    underscore: '../js/underscore'
    jquery: '../js/jquery'
    text: '../js/text'
    mustache: '../js/mustache'
    datepicker: '../js/bootstrap-datetimepicker.min'
    selectize: '../js/selectize.min'
    'mustache-wax': '../js/mustache-wax'
    'socket.io': '../js/socket.io'
    moment: '../js/moment-with-locales'
    validate: '../js/validate'
    parser: '../js/parser'
    chart: '../js/Chart.min'

require [
  'backbone'
  'views/main'
  'sockets'
  'underscore'
  'jquery'
  'moment'
  'collections/categories'
  'collections/operations'
  'collections/plans'
  'router'
  'common'
  'parser'
  'extensions'
  'api'
], (
  Backbone,
  MainView,
  Sockets,
  _,
  $,
  moment,
  Categories,
  Operations,
  Plans,
  router,
  Common,
  Parser,
  extensions,
  Api
) ->
  moment.locale 'ru'
  state.month = state.monthStart = moment(state.monthDate)
  state.monthEnd = state.month.clone().add(1, 'month').add(-1, 'second')

  socketsUrl = if state.env is 'local' then 'http://localhost:3000' else state.baseUrl + ':3000'
  Sockets.init socketsUrl, 'space1'
  view = MainView
  view.render()
  state.started = true

  Backbone.history.start()

  Categories.load ->
    Operations.load()
    Plans.load()

  # Осталось ли до начала этого месяца менее 10 дней
  tenDaysToStartOfThisMonthLeft = state.monthStart.clone().add(-10, 'days') < moment()
  # Если надо скопировать план из прошлого месяца, спросим пользователя об этом
  if state.previousMonthId \
    and state.previousMonthHasPlans \
    and (state.isCurrentMonth or tenDaysToStartOfThisMonthLeft) \
    and state.plan_copied is 0
      setTimeout ->
        Common.notify 'danger', 'В прошлом месяце у вас был план, который вы еще не скопировали. \
          Хотите ли вы его скопировать в этот месяц, чтобы не вводить заново? Вы сможете его отредактировать так, \
          как вам потребуется, и это не затронет прошлый месяц.', 'YES_NO_NOTNOW', (answer) ->
            if answer is 'yes'
              Common.confirm 'Вы уверены?', 'Вы уверены, что хотите загрузить плановые операции из прошлого месяце? \
                Это удалит все текущие планы.', (confirmed) ->
                Api.copyPlansFromPreviousMonth() if confirmed
            else
              Api.setMonthPlanCopied (-1)

      , 2000
