define [
  'jquery'
  'backbone'
  'validate'
  'mustache-wax'
  'mustache'
  'moment'
  'common'
], ($, Backbone, validate, wax, Mustache, moment, Common) ->

  # extend validate to add standard parsing
  validate.extend validate.validators.datetime,
    parse: (value, options) ->
      if _.isString(value) and options.format
        +moment(value, options.format)
      else
        +moment.utc(value)
    format: (value, options) ->
      format = if options.dateOnly then "YYYY-MM-DD" else "YYYY-MM-DD hh:mm:ss"
      return moment.utc(value).format(format)

  # set toFixed formatter in mustache
  Mustache.Formatters =
    toFixed: (str, num) ->
      if num == undefined
        num = 2
      parseFloat(str).toFixed num
    dateFormat: (str, format) ->
      date = moment(str)
      value = date.format format
      value

  $.fn.serializeHash = ->
    data = @serializeArray()
    hash = {}
    $.each data, (i, item) ->
      hash[item.name] = item.value
    hash

  $.fn.formular = ->
    convert = (input) ->
      val = $(input).val()
      if val
        try
          expr = Parser.parse(val)
          @formula = val
          $(input).val(expr.evaluate())
        catch err
          err
    @blur (e) ->
      # При уходе фокуса пытаемся выполнить формулу
      convert(this)
    @keypress (e) ->
      convert(this) if e.keyCode is Common.ENTER_KEY
    @focus (e) ->
      $(this).val(@formula) if @formula

  # Ищем запись по id, если она есть, то обновляем её с этими данными
  # Если записи нет - создаем
  Backbone.Collection.prototype.createOrUpdate = (data) ->
    throw new Error('Id is not defined for createOrUpdate') unless data.id
    if record = @get(data.id)
      for key, value of data
        if record.get(key) isnt value
          if key is 'updated_at'
            # надо это сделать тихо, чтобы не вызывать эвент
            record.set key, value, silent: true
          else
            record.set key, value
    else
      record = @add(data)
