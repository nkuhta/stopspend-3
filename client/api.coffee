'use strict'
define [
  'common'
  'underscore'
], (Common, _) ->
  {
    getMasterData: (cb) ->
      Common.getRequest 'api/master/' + state.monthId, cb

    saveIncome: (value, cb) ->
      Common.postRequest 'api/master/' + state.monthId, { income: value }, cb

    getOperations: (cb) ->
      Common.getRequest 'api/operations/' + state.monthId, cb

    addOperation: (data, cb) ->
      data.month_id = state.monthId
      Common.postRequest 'api/operations', data, cb

    updateOperation: (id, data, cb) ->
      data.id = id
      Common.putRequest 'api/operations', data, cb

    deleteOperation: (id, cb) ->
      Common.deleteRequest 'api/operations', { id: id }, cb

    getPlans: (cb) ->
      Common.getRequest 'api/plans/' + state.monthId, cb

    addPlan: (data, cb) ->
      data.month_id = state.monthId
      Common.postRequest 'api/plans', data, cb

    deletePlan: (id, cb) ->
      Common.deleteRequest 'api/plans', { id: id }, cb

    updatePlan: (id, data, cb) ->
      data.id = id
      Common.putRequest 'api/plans', data, cb

    copyPlansFromPreviousMonth: (id, data, cb) ->
      Common.postRequest 'api/copy_plans_from_prev',
        month_id: state.monthId
      , cb

    setMonthPlanCopied: (value, cb) ->
      Common.postRequest 'api/set_plan_copied',
        month_id: state.monthId
        value: value
      , cb

    getCategories: (cb) ->
      Common.getRequest 'api/categories', cb

    addCategory: (data, cb) ->
      Common.postRequest 'api/categories', data, cb

    updateCategory: (id, data, cb) ->
      data.id = id
      Common.putRequest 'api/categories', data, cb

    deleteCategory: (id, cb) ->
      Common.deleteRequest 'api/categories', { id: id }, cb

    getReport: (type, data, cb) ->
      Common.postRequest "api/report/#{type}", data, cb


  }
