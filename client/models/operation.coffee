define [ 'backbone', 'collections/categories' ], (Backbone, Categories) ->
  Backbone.Model.extend
    defaults:
      amount: 0
      category_id: null
      category_name: null
      dt: null
      avatar_url: null
      description: ''
      is_credit: false
    toJSON: ->
      data = Backbone.Model::toJSON.call(this)
      category = Categories.get data.category_id
      data.category_name = category.get 'name' if category
      data.baseUrl = state.baseUrl
      data
