define [
  'backbone'
  'moment'
  'collections/operations'
  'collections/plans'
  'collections/days',
  'calculator'
], (Backbone, moment, Operations, Plans, Days, Calculator) ->
  model = Backbone.Model.extend(
    defaults:
      income: 0
      plan_expense: 0
      spent: 0
      left: 0
      spent_today: 0
      can_spend_per_day: 0
      can_spend_per_day_future: 0
      can_spend_today: 0
      today_is_negative: false
      health: 0
    calculate: ->
      # Если сторы с операциями или с планами еще не загрузились, то не надо вызывать калькуляцию
      if Operations.loaded and Plans.loaded and state.started
        calculator = new Calculator
        calculator.calculate(this)
  )
  model = new model
  # Это синглтон
  model.listenTo Operations, 'add reset remove change', ->
    model.calculate()
  model.listenTo Plans, 'add reset remove change', ->
    model.calculate()
  model.listenTo model, 'change:income', ->
    model.calculate()
  model
