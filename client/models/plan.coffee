define [ 'backbone', 'collections/categories' ], (Backbone, Categories) ->
  Backbone.Model.extend
    defaults:
      amount: 0
      spent: 0
      category_id: null
      category_name: null
    toJSON: ->
      data = Backbone.Model::toJSON.call(this)
      category = Categories.get data.category_id
      data.category_name = category.get 'name'
      data.percentage = parseInt(data.spent / data.amount * 100)
      data
