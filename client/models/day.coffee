define [ 'backbone' ], (Backbone) ->
  Backbone.Model.extend defaults:
    dt: null
    balance: 0
    spent: 0
