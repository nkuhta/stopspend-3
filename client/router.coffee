define [
  'backbone'
  'views/main'
], (Backbone, MainView) ->
  Router = Backbone.Router.extend
    routes:
      '': 'index',
      'operations': 'operations',
      'operations/:type': 'operations',
      'plans': 'plans',
      'calendar': 'calendar',
      'categories': 'categories',
      'reports': 'reports'
      'reports/:type': 'reports'
    index: ->
      location = if state.isPastMonth then 'operations/all' else 'operations/yesterday'
      window.location.hash = location # сразу переходим в операции
    operations: (type) ->
      unless type
        location = if state.isPastMonth then 'operations/all' else 'operations/yesterday'
        return window.location.hash = location # сразу переходим в операции
      MainView.selectTab 'operations',
        filter: type || null
    plans: -> MainView.selectTab 'plans'
    calendar: -> MainView.selectTab 'calendar'
    categories: -> MainView.selectTab 'categories'
    reports: (type) ->
      MainView.selectTab 'reports',
        type: type || null

  new Router()
