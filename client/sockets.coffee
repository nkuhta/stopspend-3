define [
  'socket.io'
  'underscore'
  'backbone'
], (socketIO, _, Backbone) ->
  service = init: (url, channel) ->
    @socket = socketIO(url)

    @socket.on 'month' + state.monthId, (message) =>
      console.log '[SOCKET]:', message.data.event, message.data.data
      @trigger message.data.event, message.data.data

    @socket.on 'space' + state.spaceId, (message) =>
      console.log '[SOCKET]:', message.data.event, message.data.data
      @trigger message.data.event, message.data.data

  _.extend service, Backbone.Events
  service
