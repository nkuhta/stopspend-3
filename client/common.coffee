'use strict'
define [
  'moment'
  'views/base/message_box'
  'views/components/notification'
], (moment, MessageBox, Notification) ->
  {
    ENTER_KEY: 13

    getRequest: (url, success) ->
      $.ajax(
        url: state.baseUrl + '/' + url
        dataType: 'json'
      ).done((data) ->
        if data.success == false and data.message
          alert data.message
          console.error data.message
          console.error data.trace
        else
          # Считаем, что в остальных случаях успех
          success data
      ).fail (error) ->
        console.error error.responseText

    _request: (method, url, data, success) ->
      $.ajax(
        url: state.baseUrl + '/' + url
        data: JSON.stringify(data)
        method: method
        dataType: 'json'
        headers:
          'X-CSRF-TOKEN': state.token
          'CONTENT-TYPE': 'application/json'
      ).done((data) ->
        if data and data.success == true
          if $.isFunction(success)
            success data.data or null
        else
          if data.success == false and data.message
            alert data.message
            if !data.normal_error
              console.error data.message
              console.error data.trace
          else
            console.error 'Success:true not received from server', data
      ).fail (error) ->
        if error.status is 401 # не авторизован
          # перенаправление на логин
          window.location.replace state.baseUrl + '/login'
        else
          console.error error.responseText

    postRequest: (url, data, success) ->
      @_request 'post', url, data, success

    putRequest: (url, data, success) ->
      @_request 'put', url, data, success

    deleteRequest: (url, data, success) ->
      @_request 'delete', url, data, success

    ucfirst: (str) -> str.charAt(0).toUpperCase() + str.slice(1)

    isCurrentMonth: -> moment(state.monthDate).format('YYYYMM') is moment().format('YYYYMM')
    isPastMonth: -> parseInt(moment(state.monthDate).format('YYYYMM')) < parseInt(moment().format('YYYYMM'))
    isFutureMonth: -> parseInt(moment(state.monthDate).format('YYYYMM')) > parseInt(moment().format('YYYYMM'))

    alert: (title, message, callback) ->
      new MessageBox
        type: 'alert'
        title: title,
        message: message
        callback: callback

    confirm: (title, message, callback) ->
      new MessageBox
        type: 'prompt'
        title: title,
        message: message
        callback: callback

    notify: (type, message, buttons, callback) ->
      new Notification
        type: type
        message: message
        buttons: buttons
        callback: callback
  }
