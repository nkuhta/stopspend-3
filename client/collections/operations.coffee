define [
  'backbone'
  'underscore'
  'models/operation'
  'api'
  'sockets'
  'moment'
], (Backbone, _, Operation, Api, Sockets, moment) ->
  collection = Backbone.Collection.extend(
    model: Operation
    comparator: (chapter) ->
      time = moment(chapter.get('dt')).toDate().getTime()
      -1 * time
    initialize: ->
      @listenTo Sockets, 'operation_created', @createOrUpdate
      @listenTo Sockets, 'operation_removed', @remove
      @listenTo Sockets, 'operation_updated', @createOrUpdate
    load: (cb) ->
      Api.getOperations (data) =>
        @loaded = true
        @reset data
        cb() if _.isFunction(cb)
    onModelUpdate: (data) ->
      model = @get(data.id)
      if model
        model.set data
      return
  )
  new collection
