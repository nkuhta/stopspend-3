define [
  'backbone'
  'underscore'
  'models/category'
  'api'
  'sockets'
], (Backbone, _, Category, Api, Sockets) ->
  collection = Backbone.Collection.extend(
    model: Category
    comparator: (a, b) ->
      if a.get('name').toUpperCase() > b.get('name').toUpperCase() then  return 1
      else if a.get('name').toUpperCase() < b.get('name').toUpperCase() then  return -1
      else return 0
    initialize: ->
      @listenTo Sockets, 'category_created', @add
      @listenTo Sockets, 'category_removed', @remove
      @listenTo Sockets, 'category_updated', @onModelUpdate
    load: (cb) ->
      Api.getCategories (data) =>
        @reset data
        cb() if _.isFunction(cb)
    onModelUpdate: (data) ->
      model = @get(data.id)
      if model
        model.set data

    getOptionsSortedByUse: ->
      options = @map (record) ->
        text: record.get 'name'
        value: record.get 'name'
        use: record.get 'count_of_use'
      _.sortBy(options, 'use').reverse()
  )
  new collection
