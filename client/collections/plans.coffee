define [
  'backbone'
  'underscore'
  'models/plan'
  'api'
  'sockets'
], (Backbone, _, Plan, Api, Sockets) ->
  collection = Backbone.Collection.extend(
    model: Plan
    initialize: ->
      @listenTo Sockets, 'plan_created', @add
      @listenTo Sockets, 'plan_removed', @remove
      @listenTo Sockets, 'plan_updated', @onModelUpdate
    load: (cb) ->
      Api.getPlans (data) =>
        @loaded = true
        @reset data
        cb() if _.isFunction(cb)
    onModelUpdate: (data) ->
      model = @get(data.id)
      if model
        model.set data
  )
  new collection
