define [
  'backbone'
  'models/day'
  'api'
  'sockets'
], (Backbone, Day) ->
  collection = Backbone.Collection.extend(
    model: Day
    initialize: ->
  )
  new collection
