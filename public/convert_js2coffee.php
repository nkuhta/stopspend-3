<?php
// Ищем все *.js файлы в директории _application и комилируем их в коффе в application

$root = '_application';

$iter = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($root, RecursiveDirectoryIterator::SKIP_DOTS),
    RecursiveIteratorIterator::SELF_FIRST,
    RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
);

$paths = array($root);
foreach ($iter as $path => $dir) {
    if ($dir->isDir()) {
        $paths[] = $path;
    }
}

foreach($paths as $path) {
    foreach(scandir($path) as $file) {
        if (!is_dir($file)) {
            if (pathinfo($file, PATHINFO_EXTENSION) !== 'js') {
                continue;
            }
            $fullPath = $path . '\\' . $file;
            $coffeePath = str_replace('.js', '.coffee', $fullPath);
            $coffeePath = str_replace('_application\\', 'application\\', $coffeePath);
            $command = 'js2coffee ' . $fullPath . ' > ' . $coffeePath;
            $command = str_replace('\\', '/', $command);
            $outputDir = dirname($coffeePath);
            if (!is_dir($outputDir)) {
                mkdir($outputDir);
            }
            echo $command . PHP_EOL . PHP_EOL;
            system($command);
        }
    }
}

