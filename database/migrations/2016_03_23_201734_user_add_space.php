<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAddSpace extends Migration
{
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->integer('space_id')->nullable()->unsigned();
            $table->foreign('space_id')->references('id')->on('spaces');
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('space_id');
        });
    }
}
