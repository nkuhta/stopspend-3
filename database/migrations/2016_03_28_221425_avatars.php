<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Avatars extends Migration
{
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('avatar')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('avatar');
        });
    }
}
