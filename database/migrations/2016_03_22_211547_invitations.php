<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invitations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitations', function ($table) {
            $table->increments('id');

            $table->integer('inviter')->unsigned(); // Пригласитель
            $table->foreign('inviter')->references('id')->on('users');

            $table->integer('invited')->unsigned(); // приглашенный
            $table->foreign('invited')->references('id')->on('users');

            $table->string('status')->required();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invitations');
    }
}
