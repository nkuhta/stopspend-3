<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('/') }}">Stopspend.ru</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                {{--<li><a href="#">О сервисе</a></li>--}}
                {{--<li><a href="#">Наша миссия</a></li>--}}
                {{--<li><a href="#">Наша миссия</a></li>--}}
            </ul>
            <div class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    <a href="{{ URL::to('profile') }}" class="btn btn-default navbar-btn">Профиль</a>
                    <a href="{{ URL::to('logout') }}" class="btn btn-default navbar-btn">Выход</a>
                @else
                    <a href="{{ URL::to('registration') }}" class="btn btn-default navbar-btn">Регистрация</a>
                    <a href="{{ URL::to('login') }}" class="btn btn-default navbar-btn">Вход</a>
                @endif
            </div>
        </div>
    </div>
</nav>