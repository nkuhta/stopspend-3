@extends('main')

@section('content')
    <div class="panel col-md-8 col-md-offset-2">
        <div class="panel-heading">
            <h4>Профиль</h4>
        </div>
        <div class="panel-body">
            <form method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if($user->avatar)
                    <img src="{{ URL::to('avatar/' . $user->avatar) }}"/>
                @endif
                <div class="form-group">
                    <label>Аватарка</label>
                    <input type="file" name="avatar"/>
                </div>
                <button type="submit" class="btn btn-default">Сохранить</button>
            </form>
            <hr/>
            <h4>Смена пароля</h4>
            <form method="POST" action="{{ URL::to('profile/change_password') }}" class="form-horizontal">
                @if($message = Session::get('password_reset_message'))
                    <div class="alert alert-success">
                        {{ $message }}
                    </div>
                @endif
                @if($errors->count())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label">Пароль</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" class="form-control" placeholder="Пароль"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Пароль еще раз</label>
                    <div class="col-sm-10">
                        <input type="password" name="password_again" class="form-control" placeholder="Пароль еще раз"/>
                    </div>
                </div>
                <button type="submit" class="btn btn-default">Сменить пароль</button>
            </form>
        </div>
    </div>
@endsection
