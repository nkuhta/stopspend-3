@extends('main')
<?php $no_navbar = true; ?>

@section('content')
    <form method="POST" action="{{ action('AuthController@resetPassword') }}" class="well login">
        <div class="panel-heading">
            <h2 class="panel-title">Восстановление пароля</h2>
        </div>
        <div class="panel-body">

            @if($errors->count())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

                @if($message = Session::get('password_reset_message'))
                    <div class="alert alert-success">
                        {{ $message }}
                    </div>
                @endif

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">@</span>
                    <input type="text" name="email" class="form-control" placeholder="Адрес электронной почьы"/>
                </div>
            </div>

            <div class="buttons">
                <input type="submit" class="btn btn-success" value="Восстановить пароль"/>
            </div>
            <hr/>
            <p>
                Помните пароль? <a href="{{ URL::to('login') }}">Войти</a><br/>
                <a href="{{ URL::to('/') }}">На главную</a>
            </p>
        </div>
    </form>
@endsection