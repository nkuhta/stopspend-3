@extends('main')
<?php $no_navbar = true; ?>

@section('content')
<form method="POST" action="{{ action('AuthController@register') }}" class="panel panel-default login">
    <div class="panel-heading">
        <h2 class="panel-title">Регистрация</h2>
    </div>
    <div class="panel-body">

        @if($errors->count())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif



        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                <input type="text" name="name" class="form-control"
                       placeholder="Имя пользователя" value="{{ old('name') }}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">@</span>
                <input type="text" name="email" class="form-control"
                       placeholder="Адрес электронной почты" value="{{ old('email') }}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                <input type="password" name="password" class="form-control" placeholder="Пароль"/>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                <input type="password" name="password_again" class="form-control" placeholder="Повторите пароль"/>
            </div>
        </div>

        <div class="buttons">
            <input type="submit" class="btn btn-success" value="Зарегистрироваться"/>
        </div>
        <hr/>
        <p>
            Уже зарегистрированы? <a href="{{ URL::to('login') }}">Войдите</a><br/>
            <a href="{{ URL::to('/') }}">На главную</a>
        </p>
    </div>
</form>
@endsection