Здравствуйте!<br/><br/>

Поздравляем вас с успешной регистрацией на сайте <a href="{{ URL::to('/') }}">Stopspend.ru</a>.<br/><br/>

Поскорее уже <a href="{{ URL::to('/login/') }}">заходите</a>, и начинайте экономить!<br/><br/>

Приятной экономии,<br/>
команда <a href="{{ URL::to('/') }}">Stopspend.ru</a>