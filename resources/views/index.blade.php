@extends('main')

@section('content')
    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>
        <p>Мы верим, что Stopspend поможет любому приобрести финансовую дисциплину!</p>
        <p>
            <a class="btn btn-primary btn-lg" href="{{ URL::to('registration') }}" role="button">Зарегистрироваться</a>
            <a class="btn btn-success btn-lg" href="{{ URL::to('login') }}" role="button">Войти</a>
        </p>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-1" >
            <h3>Эффективность</h3>
            <p>В основе сервиса лежит ежедневный контроль за средствами.
                У вас не получится прогулять все деньги сразу после зарплаты, система позаботится обо всём месяце.
            </p>
        </div>
        <div class="col-md-3">
            <h3>Наглядность</h3>
            <p>
                Ваша основная цифра теперь - сколько вы можете потратить сегодня. Её вы будете видеть всегда.
            </p>
        </div>
        <div class="col-md-3">
            <h3>Технологичность</h3>
            <p>
                Сервис построен на самых передовых технологиях, что обеспечивает непревзойденную скорость работы и удобство.
            </p>
        </div>
    </div>
@endsection