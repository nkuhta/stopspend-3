<div class="row">

    @if($space->author_id !== \Auth::user()->id)
    <div class="alert alert-info col-md-offset-2 col-sm-7" role="alert">
        Вы сейчас находитесь в пространстве данных пользователя <strong>{{ $space->author()->email }}</strong>.
        Вы можете выйти из этого пространства данных и вернуться в своё в любой момент, как только захотите.
        <a href="{{ URL::to('leave_space') }}" class="alert-link">Выйти</a>
    </div>
    @endif

    <section class="panel panel-default col-md-offset-2 col-sm-7 invitations">
        <h2>
            <a href="#" class="invitation-toggle-handler"><span class="glyphicon glyphicon-chevron-down"></span></a>
            Приглашения пользователей
            @if (count($intitationsAsInvited))
            <span class="label label-danger">{{ count($intitationsAsInvited) }}</span>
            @endif
        </h2>
        <div class="invitation-content">
            <p>Вести семейный бюджет в одиночку сложновато.
                Подключите к вашему рабочему пространству супругу/супруга, чтобы вносить расходы
                и управлять своими финансы вместе. Вы можете пригласить сколько угодно пользователей, но зачем?
                Пригласите того, кто вам действительно нужен!</p>
            <hr/>
            <form method="POST" class="invite-form form-inline">
                <h4>Приглашение пользователя в ваше пространство данных</h4>
                <p>Чтобы пригласить пользователя, вам нужно ввести его адрес электронной почты.
                    Он должен быть зарегистрирован в системе.</p>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="form-group">
                    <label>Пригласить пользователя</label>
                    <input type="email" class="form-control" name="email" placeholder="Email" required/>
                </div>
                <input type="submit" class="btn btn-default" value="Пригласить"/>
            </form>
            <hr/>
            <h4>Пользователи, которых вы пригласили к себе</h4>

            @if (count($intitationsAsInviter))
                <div class="panel panel-default" style="margin: 10px;">
                <ul class="list-group">
                    @foreach($intitationsAsInviter as $invitation)
                        <?php
                            $labelMessage = '';
                            $labelClass = '';
                            switch($invitation->status) {
                                case 'new':
                                    $labelClass = 'label-default';
                                    $labelMessage = 'Решение еще не принято';
                                    break;
                                case 'accepted':
                                    $labelClass = 'label-success';
                                    $labelMessage = 'Принято';
                                    break;
                                case 'cancelled':
                                    $labelClass = 'label-danger';
                                    $labelMessage = 'Отклонено';
                                    break;
                            }
                        ?>
                        <li class="list-group-item">
                            {{ $invitation->invited()->email }}
                            <span class="label {{ $labelClass }}">{{ $labelMessage }}</span>
                        </li>
                    @endforeach
                </ul>
            </div>
            @else
                <p class="you-dont-have-invites">Вы еще никого не приглашали</p>
            @endif

            <hr/>
            <h4>Пользователи, которые пригласили вас в свои пространства данных</h4>

            @if (count($intitationsAsInvited))
                <div class="panel panel-default" style="margin: 10px;">
                    <ul class="list-group">
                        @foreach($intitationsAsInvited as $invitation)
                        <li class="list-group-item" invitation="{{ $invitation->id }}">
                            <a href="#" class="accept_invitation btn btn-primary btn-sm">Принять</a>
                            <a href="#" class="cancel_invitation btn btn-danger btn-sm">Отказаться</a>
                            {{ $invitation->inviter()->email }}
                        </li>
                        @endforeach
                    </ul>
                </div>
            @else
                <p class="you-are-not-invited">У вас нет приглашений, на которые вы не ответили</p>
            @endif
        </div>
    </section>
</div>


<script type="text/javascript">
    // @todo Унести javascript отсюда, ему тут не место
    $('form.invite-form').submit(function (e) {
        e.preventDefault();
        var form = $('form.invite-form');
        var email = $(this).find('input[type=email]').val();
        var token = $(this).find('input[name=_token]').val();
        form.find('.label-danger').remove();
        $.post('{{ URL::to('invite') }}', {email: email, _token: token}).success(function (data) {
            if (data.success === false) {
                var label = $('<span class="label label-danger"></span>').html(data.message);
                form.append(label);
            } else if (data.success === true) {
                window.location.reload();
            }
        });
    });
    $('a.accept_invitation').click(function (e) {
        e.preventDefault();
        var invitationId = $(this).parent().attr('invitation');
        $.post('{{ URL::to('invite/accept') }}', {
            _token: '{{ csrf_token() }}',
            invitation: invitationId
        }).success(function (data) {
            if (data.success === false) {
                alert(data.message);
            } else if (data.success === true) {
                window.location.reload();
            }
        });
    });

    $('a.cancel_invitation').click(function (e) {
        e.preventDefault();
        var invitationId = $(this).parent().attr('invitation');
        $.post('{{ URL::to('invite/cancel') }}', {
            _token: '{{ csrf_token() }}',
            invitation: invitationId
        }).success(function (data) {
            if (data.success === false) {
                alert(data.message);
            } else if (data.success === true) {
                window.location.reload();
            }
        });
    });

    $('a.invitation-toggle-handler').click(function (e) {
        var content = $('.invitation-content');
        if ($(content).is(':hidden')) {
            $(content).show();
            $(this).find('span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        } else {
            $(content).hide();
            $(this).find('span').addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
        }
    });
</script>