<div class="row">
    <div class="month-selection panel panel-default col-md-offset-2 col-sm-7">
        <h2>Выбор месяца</h2>
        <p>Выберите месяц, который вы хотели бы открыть.</p>
        <hr/>
        <ul>
        @foreach($months as $date => $month)
            <?php
                $health = $month['health'];
                $healthCls = 'health-grey';
                if (($health > 0 && $health < 50) || ($health == 0 && strtotime($date.'-01') < time())) {
                    $healthCls = 'health-red';
                } else if ($health >= 50 && $health < 80) {
                    $healthCls = 'health-yellow';
                } else if ($health > 80) {
                    $healthCls = 'health-green';
                }

                $balance = $month['balance'];
                $balanceCls = 'grey';
                if ($balance !== null && $balance < 0) {
                    $balanceCls = 'red';
                } else if ($balance !== null && $balance > 0) {
                    $balanceCls = 'green';
                }
            ?>
            <a href="{{ URL::action('MonthController@getMonth', $date) }}"><li class="month">
                <p class="title">{{ $month['date'] }}</p>
                <p class="health {{ $healthCls }}" title="Финансовое здоровье">{{ $health }}%</p>
                <p class="balance">Баланс:
                    <span class="summ {{ $balanceCls  }}">{{ $balance === null ? '-' : (round($balance) . '  ₽') }}</span>
                </p>
            </li></a>
        @endforeach
        </ul>
    </div>
</div>