<!DOCTYPE html>
<html>
<head>
    <title>Stopspend.ru</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('style/application.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('style/bootstrap-datetimepicker.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('style/selectize.bootstrap3.css') }}" type="text/css"/>
    <link rel="icon" type="image/png" href="{{ URL::to('ico.png') }}" />
</head>
<body>
@yield('content')

</body>
</html>