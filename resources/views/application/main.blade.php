@extends('application/base')

@section('content')
    @if($months->count())
        <p>Месяцы:</p>
        @foreach($months->get() as $month)
            <a href="{{ URL::action('MonthController@getMonth', $month->id) }}">{{ $month->date }}</a>
        @endforeach
    @else
        Еще нет ни одного месяца
        <a href="{{ URL::action('SpaceController@createCurrentMonth') }}"
    @endif
@endsection
