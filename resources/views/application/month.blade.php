@extends('application/base')

@section('content')
<script data-main="../build/main" src="{{ asset('js/require.js') }}"></script>
<script>
    window.state = {
        baseUrl: '{{ URL::to('/') }}',
        spaceId: {{ $month->space()->id }},
        monthId: {{ $month->id }},
        monthDate: '{{ $month->date }}-01',
        token: '{{ csrf_token() }}',
        env: '{{ App::environment() }}',
        inheritedBalance: {{ $month->getInheritedBalance() ? $month->getInheritedBalance() : 0}},
        previousMonthId: {{ $month->getPreviousMonth() ? $month->getPreviousMonth()->id : 'null' }},
        previousMonthHasPlans: {{ $month->getPreviousMonth() && $month->getPreviousMonth()->plans()->count() > 0 ? 'true' : 'false' }},
        plan_copied: {{ $month->plan_copied }},
        isCurrentMonth: {{ $month->isCurrentMonth() ? 'true' : 'false' }},
        isPastMonth: {{ $month->isPastMonth() ? 'true' : 'false' }},
        version: '{{ \Session::get('version') }}'
    };
</script>
@endsection