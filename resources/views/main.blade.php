<!DOCTYPE html>
<html>
<head>
        <title>Stopspend.ru</title>
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css') }}" type="text/css"/>
        <link rel="stylesheet" href="{{ asset('style/start.css') }}" type="text/css"/>
        <script type="text/javascript" src="js/jquery.js"></script>
        <link rel="icon" type="image/png" href="{{ URL::to('ico.png') }}" />
    </head>
<body>


@if(empty($no_navbar))
@include('partials/navbar')
@endif

@yield('content')
</body>
</html>