var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();
redis.subscribe('events', function(err, count) {
});
redis.on('message', function(ch, message) {
    console.log('Message Recieved: ' + message);
    message = JSON.parse(message);
    var channel = message.data.data.channel;
    delete message.data.data.channel;
    io.emit(channel, message.data);
});
http.listen(3000, function(){
    console.log('Listening on Port 3000');
});